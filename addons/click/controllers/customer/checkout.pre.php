<?php
/***************************************************************************
*                                                                          *
*   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


//
// $Id$
//

if ( !defined('AREA') ) { die('Access denied'); }

$cart = & $_SESSION['cart'];

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
    if ($mode == 'add') {
        if (fn_click_get_collect_requirement($_REQUEST)) {
            exit;
        }
    }

	return;
}

if ($mode == 'request_callback') {

	if (Registry::get('addons.click.admin_email') != '') {
		$lang_code = CART_LANGUAGE;
		$to = Registry::get('addons.click.admin_email');
		$product = fn_get_product_name($_REQUEST['product_id']);
		$body['product'] = $product;
		$body['email'] = $_REQUEST['email'];
		$body['phone'] = $_REQUEST['phone'];
		$subj = $lang['click_collect_requirment'];
		Registry::get('view_mail')->assign('body', $body);
		Registry::get('view_mail')->assign('subj', $subj);

		$_from = array(
			'email' => !empty($from['from_email']) ? $from['from_email'] : Registry::get('settings.Company.company_newsletter_email'),
			'name' => !empty($from['from_name']) ? $from['from_name'] : Registry::get('settings.Company.company_name')
		);

		$rezult = fn_send_mail($to, $_from, 'addons/click/click_subj.tpl', 'addons/click/click_body.tpl', '', $lang_code, '', true);
	}

	exit;
}
?>