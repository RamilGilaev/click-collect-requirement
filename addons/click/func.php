<?php
/***************************************************************************
*                                                                          *
*    Copyright (c) 2004 Simbirsk Technologies Ltd. All rights reserved.    *
*                                                                          *
* This  is  commercial  software,  only  users  who have purchased a valid *
* license  and  accept  to the terms of the  License Agreement can install *
* and use this program.                                                    *
*                                                                          *
****************************************************************************
* PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
* "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
****************************************************************************/


//
// $Id$
//

if ( !defined('AREA') ) { die('Access denied'); }

function fn_click_get_collect_requirement($data)
{
    foreach ($data['product_data'] as $val) {
        $is_requirement = db_get_field("SELECT requirement FROM ?:products WHERE product_id = ?i", $val['product_id']);
    }
    
    return $is_requirement == 'Y'? true : false;
}

function fn_click_get_collect_requirement_product($product_id)
{   
    $is_requirement = db_get_field("SELECT requirement FROM ?:products WHERE product_id = ?i", $product_id);
    
    return $is_requirement == 'Y'? true : false;
}

function fn_write_r() { 
    static $count = 0; 
    $args = func_get_args(); 
    $fp = fopen('ajax_result.html', 'w+'); 
    if (!empty($args)) { 
	fwrite($fp, '<ol style="font-family: Courier; font-size: 12px; border: 1px solid #dedede; background-color: #efefef; float: left; padding-right: 20px;">');
	foreach ($args as $k => $v) { 
	    $v = htmlspecialchars(print_r($v, true)); 
	    if ($v == '') { 
		$v = ' '; 
	    } fwrite($fp, '<li><pre>' . $v . "\n" . '</pre></li>'); 
	} 
	fwrite($fp, '</ol><div style="clear:left;"></div>'); 
    } 
    $count++; 
}

?>
