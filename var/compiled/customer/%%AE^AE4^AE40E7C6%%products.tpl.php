<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:20
         compiled from blocks/products.tpl */ ?>

<?php if ($this->_tpl_vars['block']['properties']['hide_add_to_cart_button'] == 'Y'): ?>
	<?php $this->assign('_show_add_to_cart', false, false); ?>
<?php else: ?>
	<?php $this->assign('_show_add_to_cart', true, false); ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['block']['properties']['hide_options'] == 'Y'): ?>
	<?php $this->assign('_show_product_options', false, false); ?>
<?php else: ?>
	<?php $this->assign('_show_product_options', true, false); ?>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/product_list_templates/products.tpl", 'smarty_include_vars' => array('products' => $this->_tpl_vars['items'],'no_sorting' => 'Y','obj_prefix' => ($this->_tpl_vars['block']['block_id'])."000",'item_number' => $this->_tpl_vars['block']['properties']['item_number'],'show_add_to_cart' => $this->_tpl_vars['_show_add_to_cart'],'show_product_options' => $this->_tpl_vars['_show_product_options'],'no_pagination' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>