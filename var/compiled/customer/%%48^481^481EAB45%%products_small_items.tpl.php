<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:06
         compiled from blocks/products_small_items.tpl */ ?>

<?php if ($this->_tpl_vars['block']['properties']['hide_add_to_cart_button'] == 'Y'): ?>
	<?php $this->assign('_show_add_to_cart', false, false); ?>
<?php else: ?>
	<?php $this->assign('_show_add_to_cart', true, false); ?>
<?php endif; ?>

<?php if ($this->_tpl_vars['block']['properties']['positions'] == 'left' || $this->_tpl_vars['block']['properties']['positions'] == 'right'): ?>
	<?php $this->assign('_show_trunc_name', 'true', false); ?>
<?php else: ?>
	<?php $this->assign('_show_name', 'true', false); ?>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/list_templates/small_items.tpl", 'smarty_include_vars' => array('products' => $this->_tpl_vars['items'],'obj_prefix' => ($this->_tpl_vars['block']['block_id'])."000",'item_number' => $this->_tpl_vars['block']['properties']['item_number'],'show_name' => $this->_tpl_vars['_show_name'],'show_trunc_name' => $this->_tpl_vars['_show_trunc_name'],'show_price' => true,'show_add_to_cart' => $this->_tpl_vars['_show_add_to_cart'],'show_list_buttons' => false,'but_role' => 'act')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>