<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:05
         compiled from addons/discussion/blocks/testimonials.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'fn_get_discussion_rating', 'addons/discussion/blocks/testimonials.tpl', 1, false),array('modifier', 'fn_get_discussion', 'addons/discussion/blocks/testimonials.tpl', 3, false),array('modifier', 'fn_get_discussion_posts', 'addons/discussion/blocks/testimonials.tpl', 7, false),array('modifier', 'fn_url', 'addons/discussion/blocks/testimonials.tpl', 13, false),array('modifier', 'truncate', 'addons/discussion/blocks/testimonials.tpl', 13, false),array('modifier', 'escape', 'addons/discussion/blocks/testimonials.tpl', 13, false),array('modifier', 'nl2br', 'addons/discussion/blocks/testimonials.tpl', 13, false),array('modifier', 'date_format', 'addons/discussion/blocks/testimonials.tpl', 16, false),array('block', 'hook', 'addons/discussion/blocks/testimonials.tpl', 16, false),)), $this); ?>
<?php
fn_preload_lang_vars(array('more_w_ellipsis'));
?>
<?php  ob_start();  ?>
<?php $this->assign('discussion', fn_get_discussion(0, 'E'), false); ?>

<?php if ($this->_tpl_vars['discussion'] && $this->_tpl_vars['discussion']['type'] != 'D'): ?>

<?php $this->assign('posts', fn_get_discussion_posts($this->_tpl_vars['discussion']['thread_id'], 0, $this->_tpl_vars['block']['properties']['limit'], $this->_tpl_vars['block']['properties']['random']), false); ?>

<?php if ($this->_tpl_vars['posts']): ?>
<?php $_from = $this->_tpl_vars['posts']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['post']):
?>

<?php if ($this->_tpl_vars['discussion']['type'] == 'C' || $this->_tpl_vars['discussion']['type'] == 'B'): ?>
	<p class="post-message"><a href="<?php echo fn_url("discussion.view?thread_id=".($this->_tpl_vars['discussion']['thread_id'])."&amp;post_id=".($this->_tpl_vars['post']['post_id'])); ?>
">"<?php echo smarty_modifier_nl2br(smarty_modifier_escape(smarty_modifier_truncate($this->_tpl_vars['post']['message'], 100), 'html')); ?>
"</a></p>
<?php endif; ?>

<p class="post-author">&ndash; <?php echo $this->_tpl_vars['post']['name']; ?>
<?php $this->_tag_stack[] = array('hook', array('name' => "discussion:block_items_list_row")); $_block_repeat=true;smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?><?php if ($this->_tpl_vars['block']['properties']['positions'] != 'left' && $this->_tpl_vars['block']['properties']['positions'] != 'right'): ?>, <em><?php echo smarty_modifier_date_format($this->_tpl_vars['post']['timestamp'], ($this->_tpl_vars['settings']['Appearance']['date_format'])); ?>
</em><?php endif; ?><?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?></p>

<?php if ($this->_tpl_vars['block']['properties']['positions'] != 'left' && $this->_tpl_vars['block']['properties']['positions'] != 'right'): ?>
<div class="clear">
	<div class="right"></div>
	<?php if ($this->_tpl_vars['discussion']['type'] == 'R' || $this->_tpl_vars['discussion']['type'] == 'B'): ?>
		<div class="right"><?php $__parent_tpl_vars = $this->_tpl_vars;$this->_tpl_vars = array_merge($this->_tpl_vars, array('stars' => fn_get_discussion_rating($this->_tpl_vars['post']['rating_value']), )); ?>

<p class="nowrap stars">
<?php if ($this->_tpl_vars['controller'] == 'products' && $this->_tpl_vars['mode'] == 'view'): ?><a onclick="$('#block_discussion').click(); return false;"><?php endif; ?>
<?php unset($this->_sections['full_star']);
$this->_sections['full_star']['name'] = 'full_star';
$this->_sections['full_star']['loop'] = is_array($_loop=$this->_tpl_vars['stars']['full']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['full_star']['show'] = true;
$this->_sections['full_star']['max'] = $this->_sections['full_star']['loop'];
$this->_sections['full_star']['step'] = 1;
$this->_sections['full_star']['start'] = $this->_sections['full_star']['step'] > 0 ? 0 : $this->_sections['full_star']['loop']-1;
if ($this->_sections['full_star']['show']) {
    $this->_sections['full_star']['total'] = $this->_sections['full_star']['loop'];
    if ($this->_sections['full_star']['total'] == 0)
        $this->_sections['full_star']['show'] = false;
} else
    $this->_sections['full_star']['total'] = 0;
if ($this->_sections['full_star']['show']):

            for ($this->_sections['full_star']['index'] = $this->_sections['full_star']['start'], $this->_sections['full_star']['iteration'] = 1;
                 $this->_sections['full_star']['iteration'] <= $this->_sections['full_star']['total'];
                 $this->_sections['full_star']['index'] += $this->_sections['full_star']['step'], $this->_sections['full_star']['iteration']++):
$this->_sections['full_star']['rownum'] = $this->_sections['full_star']['iteration'];
$this->_sections['full_star']['index_prev'] = $this->_sections['full_star']['index'] - $this->_sections['full_star']['step'];
$this->_sections['full_star']['index_next'] = $this->_sections['full_star']['index'] + $this->_sections['full_star']['step'];
$this->_sections['full_star']['first']      = ($this->_sections['full_star']['iteration'] == 1);
$this->_sections['full_star']['last']       = ($this->_sections['full_star']['iteration'] == $this->_sections['full_star']['total']);
?><img src="<?php echo $this->_tpl_vars['images_dir']; ?>
/icons/star_full.gif" width="13" height="12" alt="*" /><?php endfor; endif; ?>
<?php if ($this->_tpl_vars['stars']['part']): ?><img src="<?php echo $this->_tpl_vars['images_dir']; ?>
/icons/star_<?php echo $this->_tpl_vars['stars']['part']; ?>
.gif" width="13" height="12" alt="" /><?php endif; ?>
<?php unset($this->_sections['full_star']);
$this->_sections['full_star']['name'] = 'full_star';
$this->_sections['full_star']['loop'] = is_array($_loop=$this->_tpl_vars['stars']['empty']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['full_star']['show'] = true;
$this->_sections['full_star']['max'] = $this->_sections['full_star']['loop'];
$this->_sections['full_star']['step'] = 1;
$this->_sections['full_star']['start'] = $this->_sections['full_star']['step'] > 0 ? 0 : $this->_sections['full_star']['loop']-1;
if ($this->_sections['full_star']['show']) {
    $this->_sections['full_star']['total'] = $this->_sections['full_star']['loop'];
    if ($this->_sections['full_star']['total'] == 0)
        $this->_sections['full_star']['show'] = false;
} else
    $this->_sections['full_star']['total'] = 0;
if ($this->_sections['full_star']['show']):

            for ($this->_sections['full_star']['index'] = $this->_sections['full_star']['start'], $this->_sections['full_star']['iteration'] = 1;
                 $this->_sections['full_star']['iteration'] <= $this->_sections['full_star']['total'];
                 $this->_sections['full_star']['index'] += $this->_sections['full_star']['step'], $this->_sections['full_star']['iteration']++):
$this->_sections['full_star']['rownum'] = $this->_sections['full_star']['iteration'];
$this->_sections['full_star']['index_prev'] = $this->_sections['full_star']['index'] - $this->_sections['full_star']['step'];
$this->_sections['full_star']['index_next'] = $this->_sections['full_star']['index'] + $this->_sections['full_star']['step'];
$this->_sections['full_star']['first']      = ($this->_sections['full_star']['iteration'] == 1);
$this->_sections['full_star']['last']       = ($this->_sections['full_star']['iteration'] == $this->_sections['full_star']['total']);
?><img src="<?php echo $this->_tpl_vars['images_dir']; ?>
/icons/star_empty.gif" width="13" height="12" alt="" /><?php endfor; endif; ?>
<?php if ($this->_tpl_vars['controller'] == 'products' && $this->_tpl_vars['mode'] == 'view'): ?></a><?php endif; ?>
</p><?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?></div>
	<?php endif; ?>
</div>
<?php endif; ?>

<?php endforeach; endif; unset($_from); ?>

<div class="right">
	<a href="<?php echo fn_url("discussion.view?thread_id=".($this->_tpl_vars['discussion']['thread_id'])); ?>
"><?php echo fn_get_lang_var('more_w_ellipsis', $this->getLanguage()); ?>
</a>
</div>
<?php endif; ?>

<?php endif; ?><?php  ob_end_flush();  ?>