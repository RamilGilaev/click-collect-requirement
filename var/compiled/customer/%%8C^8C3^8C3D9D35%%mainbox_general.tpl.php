<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:13
         compiled from blocks/wrappers/mainbox_general.tpl */ ?>
<?php  ob_start();  ?><?php if ($this->_tpl_vars['anchor']): ?>
<a name="<?php echo $this->_tpl_vars['anchor']; ?>
"></a>
<?php endif; ?>
<div class="mainbox-container<?php if ($this->_tpl_vars['details_page']): ?> details-page<?php endif; ?>">
	<?php if ($this->_tpl_vars['title']): ?>
	<h1 class="mainbox-title"><span><?php echo $this->_tpl_vars['title']; ?>
</span></h1>
	<?php endif; ?>
	<div class="mainbox-body"><?php echo $this->_tpl_vars['content']; ?>
</div>
</div>
<?php  ob_end_flush();  ?>