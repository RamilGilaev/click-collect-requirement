<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:09
         compiled from blocks/my_account.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'blocks/my_account.tpl', 7, false),array('modifier', 'fn_url', 'blocks/my_account.tpl', 11, false),array('modifier', 'default', 'blocks/my_account.tpl', 27, false),array('modifier', 'fn_needs_image_verification', 'blocks/my_account.tpl', 63, false),array('modifier', 'fn_get_settings', 'blocks/my_account.tpl', 64, false),array('modifier', 'uniqid', 'blocks/my_account.tpl', 67, false),array('block', 'hook', 'blocks/my_account.tpl', 9, false),)), $this); ?>
<?php
fn_preload_lang_vars(array('profile_details','downloads','sign_in','register','orders','my_tags','return_requests','my_points','wishlist','rb_subscriptions','sign_out','apply_for_vendor_account','track_my_order','track_my_order','order_id','email','go','image_verification_body'));
?>
<?php  ob_start();  ?>
<?php if ($this->_tpl_vars['auth']['user_id']): ?>
<strong><?php echo $this->_tpl_vars['user_info']['firstname']; ?>
 <?php echo $this->_tpl_vars['user_info']['lastname']; ?>
</strong>
<?php endif; ?>

<?php $this->assign('return_current_url', smarty_modifier_escape($this->_tpl_vars['config']['current_url'], 'url'), false); ?>
<ul class="arrows-list">
<?php $this->_tag_stack[] = array('hook', array('name' => "profiles:my_account_menu")); $_block_repeat=true;smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
	<?php if ($this->_tpl_vars['auth']['user_id']): ?>
		<li><a href="<?php echo fn_url("profiles.update"); ?>
" rel="nofollow" class="underlined"><?php echo fn_get_lang_var('profile_details', $this->getLanguage()); ?>
</a></li>
		<li><a href="<?php echo fn_url("orders.downloads"); ?>
" rel="nofollow" class="underlined"><?php echo fn_get_lang_var('downloads', $this->getLanguage()); ?>
</a></li>
	<?php else: ?>
		<li><a href="<?php if ($this->_tpl_vars['controller'] == 'auth' && $this->_tpl_vars['mode'] == 'login_form'): ?><?php echo fn_url($this->_tpl_vars['config']['current_url']); ?>
<?php else: ?><?php echo fn_url("auth.login_form?return_url=".($this->_tpl_vars['return_current_url'])); ?>
<?php endif; ?>" rel="nofollow" class="underlined"><?php echo fn_get_lang_var('sign_in', $this->getLanguage()); ?>
</a> / <a href="<?php echo fn_url("profiles.add"); ?>
" rel="nofollow" class="underlined"><?php echo fn_get_lang_var('register', $this->getLanguage()); ?>
</a></li>
	<?php endif; ?>
	<li><a href="<?php echo fn_url("orders.search"); ?>
" rel="nofollow" class="underlined"><?php echo fn_get_lang_var('orders', $this->getLanguage()); ?>
</a></li>
<?php if ($this->_tpl_vars['addons']['tags']['status'] == 'A'): ?><?php $__parent_tpl_vars = $this->_tpl_vars; ?>

<li><a href="<?php echo fn_url("tags.summary"); ?>
" rel="nofollow"><?php echo fn_get_lang_var('my_tags', $this->getLanguage()); ?>
</a></li>
<?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?><?php endif; ?><?php if ($this->_tpl_vars['addons']['rma']['status'] == 'A'): ?><?php $__parent_tpl_vars = $this->_tpl_vars; ?>

<li><a href="<?php echo fn_url("rma.returns"); ?>
" rel="nofollow"><?php echo fn_get_lang_var('return_requests', $this->getLanguage()); ?>
</a></li>

<?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?><?php endif; ?><?php if ($this->_tpl_vars['addons']['reward_points']['status'] == 'A'): ?><?php $__parent_tpl_vars = $this->_tpl_vars; ?>

<?php if ($this->_tpl_vars['auth']['user_id']): ?>
<li><a href="<?php echo fn_url("reward_points.userlog"); ?>
" rel="nofollow"><?php echo fn_get_lang_var('my_points', $this->getLanguage()); ?>
:&nbsp;<strong><?php echo smarty_modifier_default(@$this->_tpl_vars['user_info']['points'], '0'); ?>
</strong></a></li>
<?php endif; ?><?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?><?php endif; ?><?php if ($this->_tpl_vars['addons']['wishlist']['status'] == 'A'): ?><?php $__parent_tpl_vars = $this->_tpl_vars; ?>

<li><a href="<?php echo fn_url("wishlist.view"); ?>
" rel="nofollow"><?php echo fn_get_lang_var('wishlist', $this->getLanguage()); ?>
</a></li>
<?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?><?php endif; ?><?php if ($this->_tpl_vars['addons']['recurring_billing']['status'] == 'A'): ?><?php $__parent_tpl_vars = $this->_tpl_vars; ?>

<li><a href="<?php echo fn_url("subscriptions.search"); ?>
" rel="nofollow"><?php echo fn_get_lang_var('rb_subscriptions', $this->getLanguage()); ?>
</a></li>

<?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?><?php endif; ?><?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>

<?php if ($this->_tpl_vars['auth']['user_id']): ?>
		<li class="delim"></li>
		<li><a href="<?php echo fn_url("auth.logout?redirect_url=".($this->_tpl_vars['return_current_url'])); ?>
" rel="nofollow" class="underlined"><?php echo fn_get_lang_var('sign_out', $this->getLanguage()); ?>
</a></li>
<?php endif; ?>

<?php if ($this->_tpl_vars['settings']['Suppliers']['apply_for_vendor'] == 'Y' && $this->_tpl_vars['controller'] != 'companies' && $this->_tpl_vars['mode'] != 'apply_for_vendor' && ! $this->_tpl_vars['user_info']['company_id']): ?>
	<li><a href="<?php echo fn_url("companies.apply_for_vendor?return_previous_url=".($this->_tpl_vars['return_current_url'])); ?>
" rel="nofollow" class="underlined"><?php echo fn_get_lang_var('apply_for_vendor_account', $this->getLanguage()); ?>
</a></li><?php endif; ?>
</ul>

<div class="updates-wrapper" id="track_orders">

<form action="<?php echo fn_url(""); ?>
" method="get" class="cm-ajax" name="track_order_quick">
<input type="hidden" name="result_ids" value="track_orders" />

<p><?php echo fn_get_lang_var('track_my_order', $this->getLanguage()); ?>
:</p>

<div class="form-field">
<label for="track_order_item<?php echo $this->_tpl_vars['block']['block_id']; ?>
" class="cm-required hidden"><?php echo fn_get_lang_var('track_my_order', $this->getLanguage()); ?>
:</label>
	<div class="float-left"><input type="text" size="20" class="input-text cm-hint" style="float: left;" id="track_order_item<?php echo $this->_tpl_vars['block']['block_id']; ?>
" name="track_data" value="<?php echo smarty_modifier_escape(fn_get_lang_var('order_id', $this->getLanguage()), 'html'); ?>
<?php if (! $this->_tpl_vars['auth']['user_id']): ?>/<?php echo smarty_modifier_escape(fn_get_lang_var('email', $this->getLanguage()), 'html'); ?>
<?php endif; ?>" /></div>
	<div class="float-right"><?php $__parent_tpl_vars = $this->_tpl_vars;$this->_tpl_vars = array_merge($this->_tpl_vars, array('but_name' => "orders.track_request", 'alt' => fn_get_lang_var('go', $this->getLanguage()), )); ?>
<input type="image" src="<?php echo $this->_tpl_vars['images_dir']; ?>
/icons/go.gif" alt="<?php echo $this->_tpl_vars['alt']; ?>
" title="<?php echo $this->_tpl_vars['alt']; ?>
" class="go-button" />
<input type="hidden" name="dispatch" value="<?php echo $this->_tpl_vars['but_name']; ?>
" /><?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?></div>
	<div class="clear-both"></div>
	<?php if ($this->_tpl_vars['settings']['Image_verification']['use_for_track_orders'] == 'Y'): ?>
		<?php $__parent_tpl_vars = $this->_tpl_vars;$this->_tpl_vars = array_merge($this->_tpl_vars, array('id' => 'track_orders', 'align' => 'left', 'sidebox' => true, )); ?>

<?php if (fn_needs_image_verification("") == true): ?>
	<?php $this->assign('is', fn_get_settings('Image_verification'), false); ?>
	
	<p<?php if ($this->_tpl_vars['align']): ?> class="<?php echo $this->_tpl_vars['align']; ?>
"<?php endif; ?>><?php echo fn_get_lang_var('image_verification_body', $this->getLanguage()); ?>
</p>
	<?php $this->assign('id_uniqid', uniqid($this->_tpl_vars['id']), false); ?>
	<?php if ($this->_tpl_vars['sidebox']): ?>
		<p><img id="verification_image_<?php echo $this->_tpl_vars['id']; ?>
" class="image-captcha valign" src="<?php echo fn_url("image.captcha?verification_id=".($this->_tpl_vars['SESS_ID']).":".($this->_tpl_vars['id'])."&amp;".($this->_tpl_vars['id_uniqid'])."&amp;", 'C', 'rel', '&amp;'); ?>
" alt="" onclick="this.src += 'reload' ;" width="<?php echo $this->_tpl_vars['is']['width']; ?>
" height="<?php echo $this->_tpl_vars['is']['height']; ?>
" /></p>
	<?php endif; ?>

	<p><input class="captcha-input-text valign cm-autocomplete-off" type="text" name="verification_answer" value= "" />
	<?php if (! $this->_tpl_vars['sidebox']): ?>
		<img id="verification_image_<?php echo $this->_tpl_vars['id']; ?>
" class="image-captcha valign" src="<?php echo fn_url("image.captcha?verification_id=".($this->_tpl_vars['SESS_ID']).":".($this->_tpl_vars['id'])."&amp;".($this->_tpl_vars['id_uniqid'])."&amp;", 'C', 'rel', '&amp;'); ?>
" alt="" onclick="this.src += 'reload' ;"  width="<?php echo $this->_tpl_vars['is']['width']; ?>
" height="<?php echo $this->_tpl_vars['is']['height']; ?>
" />
	<?php endif; ?></p>
<?php endif; ?>
<?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?>
	<?php endif; ?>
</div>

</form>

<!--track_orders--></div><?php  ob_end_flush();  ?>