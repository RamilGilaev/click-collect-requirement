<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:03
         compiled from main.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'block', 'main.tpl', 3, false),array('modifier', 'trim', 'main.tpl', 7, false),array('modifier', 'sizeof', 'main.tpl', 25, false),array('modifier', 'fn_url', 'main.tpl', 33, false),array('modifier', 'unescape', 'main.tpl', 33, false),array('modifier', 'strip_tags', 'main.tpl', 33, false),array('modifier', 'escape', 'main.tpl', 33, false),array('modifier', 'defined', 'main.tpl', 43, false),array('modifier', 'fn_get_notifications', 'main.tpl', 44, false),array('modifier', 'lower', 'main.tpl', 60, false),array('block', 'hook', 'main.tpl', 8, false),)), $this); ?>
<?php
fn_preload_lang_vars(array('close','close','close','close','close','close'));
?>

<?php echo smarty_function_block(array('group' => 'top','assign' => 'top'), $this);?>

<?php echo smarty_function_block(array('group' => 'left','assign' => 'left'), $this);?>

<?php echo smarty_function_block(array('group' => 'right','assign' => 'right'), $this);?>

<?php echo smarty_function_block(array('group' => 'bottom','assign' => 'bottom'), $this);?>

<div id="container" class="container<?php if (! trim($this->_tpl_vars['left']) && ! trim($this->_tpl_vars['right'])): ?>-long<?php elseif (! trim($this->_tpl_vars['left'])): ?>-left<?php elseif (! trim($this->_tpl_vars['right'])): ?>-right<?php endif; ?>">
	<?php $this->_tag_stack[] = array('hook', array('name' => "index:main_content")); $_block_repeat=true;smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
	<div id="header"><?php $_smarty_tpl_vars = $this->_tpl_vars;$this->_smarty_include(array('smarty_include_tpl_file' => "top.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?></div>
	<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
	
	<div id="content">
		<div class="content-helper clear">
			<?php if (trim($this->_tpl_vars['top'])): ?>
			<div class="header">
				<?php echo $this->_tpl_vars['top']; ?>

			</div>
			<?php endif; ?>
			
			<?php $this->_tag_stack[] = array('hook', array('name' => "index:columns")); $_block_repeat=true;smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], null, $this, $_block_repeat);while ($_block_repeat) { ob_start(); ?>
			<div class="central-column">
				<div class="central-content">
					<?php $__parent_tpl_vars = $this->_tpl_vars; ?>

<?php if ($this->_tpl_vars['breadcrumbs'] && sizeof($this->_tpl_vars['breadcrumbs']) > 1): ?>
	<div class="breadcrumbs">
		<?php echo ''; ?><?php $_from = $this->_tpl_vars['breadcrumbs']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['bcn'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['bcn']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['bc']):
        $this->_foreach['bcn']['iteration']++;
?><?php echo ''; ?><?php if ($this->_tpl_vars['key'] != '0'): ?><?php echo '<img src="'; ?><?php echo $this->_tpl_vars['images_dir']; ?><?php echo '/icons/breadcrumbs_arrow.gif" class="bc-arrow" border="0" alt="&gt;" />'; ?><?php endif; ?><?php echo ''; ?><?php if ($this->_tpl_vars['bc']['link']): ?><?php echo '<a href="'; ?><?php echo fn_url($this->_tpl_vars['bc']['link']); ?><?php echo '"'; ?><?php if ($this->_tpl_vars['additional_class']): ?><?php echo ' class="'; ?><?php echo $this->_tpl_vars['additional_class']; ?><?php echo '"'; ?><?php endif; ?><?php echo '>'; ?><?php echo smarty_modifier_escape(smarty_modifier_strip_tags(smarty_modifier_unescape($this->_tpl_vars['bc']['title'])), 'html'); ?><?php echo '</a>'; ?><?php else: ?><?php echo ''; ?><?php echo smarty_modifier_escape(smarty_modifier_strip_tags(smarty_modifier_unescape($this->_tpl_vars['bc']['title'])), 'html'); ?><?php echo ''; ?><?php endif; ?><?php echo ''; ?><?php endforeach; endif; unset($_from); ?><?php echo ''; ?>

	</div>
<?php endif; ?><?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?>
					<?php $__parent_tpl_vars = $this->_tpl_vars; ?>

<?php if (! defined('AJAX_REQUEST')): ?>
<?php $_from = fn_get_notifications(""); if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['message']):
?>
<?php if ($this->_tpl_vars['message']['type'] != 'P' && $this->_tpl_vars['message']['type'] != 'L' && $this->_tpl_vars['message']['type'] != 'C'): ?>
	<?php if ($this->_tpl_vars['message']['type'] == 'O'): ?>
		<?php ob_start(); ?>
		<?php echo $this->_smarty_vars['capture']['checkout_error_content']; ?>

		<div class="error-box-container notification-content" id="notification_<?php echo $this->_tpl_vars['key']; ?>
">
			<div class="error-box">
				<img id="close_notification_<?php echo $this->_tpl_vars['key']; ?>
" class="cm-notification-close hand" src="<?php echo $this->_tpl_vars['images_dir']; ?>
/icons/notification_close.gif" width="10" height="19" border="0" alt="<?php echo fn_get_lang_var('close', $this->getLanguage()); ?>
" title="<?php echo fn_get_lang_var('close', $this->getLanguage()); ?>
" />
				<p><?php echo $this->_tpl_vars['message']['message']; ?>
</p>
			</div>
		</div>
		<?php $this->_smarty_vars['capture']['checkout_error_content'] = ob_get_contents(); ob_end_clean(); ?>
	<?php else: ?>
		<?php ob_start(); ?>
		<?php echo $this->_smarty_vars['capture']['notification_content']; ?>

		<div class="notification-content<?php if ($this->_tpl_vars['message']['message_state'] == 'I'): ?> cm-auto-hide<?php endif; ?><?php if ($this->_tpl_vars['message']['message_state'] == 'S'): ?> cm-ajax-close-notification<?php endif; ?>" id="notification_<?php echo $this->_tpl_vars['key']; ?>
">
			<div class="notification-<?php echo smarty_modifier_lower($this->_tpl_vars['message']['type']); ?>
">
				<img id="close_notification_<?php echo $this->_tpl_vars['key']; ?>
" class="cm-notification-close hand" src="<?php echo $this->_tpl_vars['images_dir']; ?>
/icons/icon_close.gif" width="13" height="13" border="0" alt="<?php echo fn_get_lang_var('close', $this->getLanguage()); ?>
" title="<?php echo fn_get_lang_var('close', $this->getLanguage()); ?>
" />
				<div class="notification-header-<?php echo smarty_modifier_lower($this->_tpl_vars['message']['type']); ?>
"><?php echo $this->_tpl_vars['message']['title']; ?>
</div>
				<div>
					<?php echo $this->_tpl_vars['message']['message']; ?>

				</div>
			</div>
		</div>
		<?php $this->_smarty_vars['capture']['notification_content'] = ob_get_contents(); ob_end_clean(); ?>
	<?php endif; ?>
<?php else: ?>
	<div class="product-notification-container<?php if ($this->_tpl_vars['message']['message_state'] == 'I'): ?> cm-auto-hide<?php endif; ?><?php if ($this->_tpl_vars['message']['message_state'] == 'S'): ?> cm-ajax-close-notification<?php endif; ?>" id="notification_<?php echo $this->_tpl_vars['key']; ?>
">
		<div class="w-shadow"></div>
		<div class="e-shadow"></div>
		<div class="nw-shadow"></div>
		<div class="ne-shadow"></div>
		<div class="sw-shadow"></div>
		<div class="se-shadow"></div>
		<div class="n-shadow"></div>
		<div class="popupbox-closer"><img id="close_notification_<?php echo $this->_tpl_vars['key']; ?>
" src="<?php echo $this->_tpl_vars['images_dir']; ?>
/icons/close_popupbox.png" class="cm-notification-close" title="<?php echo fn_get_lang_var('close', $this->getLanguage()); ?>
" alt="<?php echo fn_get_lang_var('close', $this->getLanguage()); ?>
" /></div>
		<div class="product-notification notification-correct">
			<h1><?php echo $this->_tpl_vars['message']['title']; ?>
</h1>
			<?php echo $this->_tpl_vars['message']['message']; ?>

		</div>
		<div class="s-shadow"></div>
	</div>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

<?php echo $this->_smarty_vars['capture']['notification_content']; ?>

<div class="cm-notification-container"></div>
<?php endif; ?><?php if (isset($__parent_tpl_vars)) { $this->_tpl_vars = $__parent_tpl_vars; unset($__parent_tpl_vars);} ?>
					
					<?php echo smarty_function_block(array('group' => 'central'), $this);?>

				</div>
			</div>
		
			<?php if (trim($this->_tpl_vars['left'])): ?>
			<div class="left-column">
				<?php echo $this->_tpl_vars['left']; ?>

			</div>
			<?php endif; ?>
			
			<?php if (trim($this->_tpl_vars['right'])): ?>
			<div class="right-column">
				<?php echo $this->_tpl_vars['right']; ?>

			</div>
			<?php endif; ?>
			<?php $_block_content = ob_get_contents(); ob_end_clean(); $_block_repeat=false;echo smarty_block_hook($this->_tag_stack[count($this->_tag_stack)-1][1], $_block_content, $this, $_block_repeat); }  array_pop($this->_tag_stack); ?>
			
			<?php if (trim($this->_tpl_vars['bottom'])): ?>
			<div class="bottom clear-both">
				<?php echo $this->_tpl_vars['bottom']; ?>

			</div>
			<?php endif; ?>
		</div>
	</div>
	
	<div id="footer">
		<div class="footer-helper-container">
			<div class="footer-top-helper"><span class="float-left">&nbsp;</span><span class="float-right">&nbsp;</span></div>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;$this->_smarty_include(array('smarty_include_tpl_file' => "bottom.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<div class="footer-bottom-helper"><span class="float-left">&nbsp;</span><span class="float-right">&nbsp;</span></div>
		</div>
	</div>
</div>