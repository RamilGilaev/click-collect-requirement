<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:23
         compiled from blocks/products_multicolumns_small.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'script', 'blocks/products_multicolumns_small.tpl', 4, false),)), $this); ?>

<?php echo smarty_function_script(array('src' => "js/exceptions.js"), $this);?>


<?php if ($this->_tpl_vars['block']['properties']['hide_add_to_cart_button'] == 'Y'): ?>
	<?php $this->assign('_show_add_to_cart', false, false); ?>
<?php else: ?>
	<?php $this->assign('_show_add_to_cart', true, false); ?>
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;$this->_smarty_include(array('smarty_include_tpl_file' => "blocks/list_templates/small_list.tpl", 'smarty_include_vars' => array('products' => $this->_tpl_vars['items'],'columns' => $this->_tpl_vars['block']['properties']['number_of_columns'],'form_prefix' => 'block_manager','no_sorting' => 'Y','no_pagination' => 'Y','obj_prefix' => ($this->_tpl_vars['block']['block_id'])."000",'item_number' => $this->_tpl_vars['block']['properties']['item_number'],'show_trunc_name' => true,'show_price' => true,'show_add_to_cart' => $this->_tpl_vars['_show_add_to_cart'],'show_list_buttons' => false)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>