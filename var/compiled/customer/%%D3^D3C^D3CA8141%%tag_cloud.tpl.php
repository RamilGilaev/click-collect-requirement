<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:25
         compiled from addons/tags/blocks/tag_cloud.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'addons/tags/blocks/tag_cloud.tpl', 6, false),array('modifier', 'fn_url', 'addons/tags/blocks/tag_cloud.tpl', 7, false),)), $this); ?>
<?php  ob_start();  ?>
<?php if ($this->_tpl_vars['items']): ?>
<?php $_from = $this->_tpl_vars['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['tag']):
?>
	<?php $this->assign('tag_name', smarty_modifier_escape($this->_tpl_vars['tag']['tag'], 'url'), false); ?>
	<a href="<?php echo fn_url("tags.view?tag=".($this->_tpl_vars['tag_name'])); ?>
" class="tag-level-<?php echo $this->_tpl_vars['tag']['level']; ?>
"><?php echo $this->_tpl_vars['tag']['tag']; ?>
</a>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?><?php  ob_end_flush();  ?>