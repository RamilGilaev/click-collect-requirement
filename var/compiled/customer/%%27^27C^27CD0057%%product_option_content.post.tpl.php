<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:23
         compiled from addons/recurring_billing/hooks/products/product_option_content.post.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'fn_cart_is_empty', 'addons/recurring_billing/hooks/products/product_option_content.post.tpl', 3, false),)), $this); ?>

<?php if ($this->_tpl_vars['product']['recurring_plans'] && ! fn_cart_is_empty($this->_tpl_vars['wishlist']) && $this->_tpl_vars['wishlist']['products'][$this->_tpl_vars['key']]['extra']['recurring_plan']): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;$this->_smarty_include(array('smarty_include_tpl_file' => "addons/recurring_billing/views/products/components/recurring_plan.tpl", 'smarty_include_vars' => array('plan_item' => $this->_tpl_vars['wishlist']['products'][$this->_tpl_vars['key']]['extra']['recurring_plan'],'show_radio' => false,'p_id' => $this->_tpl_vars['key'],'alt_duration' => $this->_tpl_vars['wishlist']['products'][$this->_tpl_vars['key']]['extra']['recurring_duration'],'active_item' => true,'hide_plan_id' => true)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php endif; ?>