<?php /* Smarty version 2.6.18, created on 2015-05-24 22:48:05
         compiled from blocks/products_text_links.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'fn_url', 'blocks/products_text_links.tpl', 10, false),array('modifier', 'unescape', 'blocks/products_text_links.tpl', 10, false),array('modifier', 'strip_tags', 'blocks/products_text_links.tpl', 10, false),array('modifier', 'truncate', 'blocks/products_text_links.tpl', 10, false),)), $this); ?>
<?php  ob_start();  ?>
<<?php if ($this->_tpl_vars['block']['properties']['item_number'] == 'Y'): ?>ol<?php else: ?>ul<?php endif; ?> class="bullets-list">

<?php $_from = $this->_tpl_vars['items']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['product']):
?>
<?php $this->assign('obj_id', ($this->_tpl_vars['block']['block_id'])."000".($this->_tpl_vars['product']['product_id']), false); ?>
<?php if ($this->_tpl_vars['product']): ?>
	<li>
		<a href="<?php echo fn_url("products.view?product_id=".($this->_tpl_vars['product']['product_id'])); ?>
"<?php if ($this->_tpl_vars['block']['properties']['positions'] == 'left' || $this->_tpl_vars['block']['properties']['positions'] == 'right'): ?> title="<?php echo $this->_tpl_vars['product']['product']; ?>
"><?php echo smarty_modifier_truncate(smarty_modifier_strip_tags(smarty_modifier_unescape($this->_tpl_vars['product']['product'])), 40, "...", true); ?>
<?php else: ?>><?php echo smarty_modifier_unescape($this->_tpl_vars['product']['product']); ?>
<?php endif; ?></a>
	</li>
<?php endif; ?>
<?php endforeach; endif; unset($_from); ?>

</<?php if ($this->_tpl_vars['block']['properties']['item_number'] == 'Y'): ?>ol<?php else: ?>ul<?php endif; ?>>
<?php  ob_end_flush();  ?>