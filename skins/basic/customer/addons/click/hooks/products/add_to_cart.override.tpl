

{assign var="is_requirement" value=$obj_id|fn_click_get_collect_requirement_product}
{if $is_requirement}
				{if $product.has_options && !$show_product_options && !$details_page}
					{include file="buttons/button.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_text=$lang.select_options but_href="products.view?product_id=`$product.product_id`" but_role="text" but_name=""}
				{else}
					{if $extra_button}{$extra_button}&nbsp;{/if}
					{include file="buttons/add_to_cart.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_name="dispatch[checkout.add..`$obj_id`]" but_role=$but_role block_width=$block_width obj_id=$obj_id product=$product but_onclick="fn_open_click_box('`$product.product`');"}
					{assign var="cart_button_exists" value=true}
				{/if}

{literal}
<style>
.click-popap {
    position: fixed;
    background-color: white;
    border: 1px solid black;
    width: 400px;
    max-width: 400px;
}

.overlay{
    background:transparent url(images/overlay.png) repeat top left;
    position:fixed;
    top:0px;
    bottom:0px;
    left:0px;
    right:0px;
    z-index:100;
}

.product-popap {
    font-size: 17px !important;
    text-align: justify;
    width: 280px;
    font-family: Arial;
    font-weight:bold;
    color: black;
    margin: 0px;
    padding: 0px;
    
}

.table-request-callback{
    margin: 20px;
    width: 280px;
    font-size: 3px;
    white-space: pre-wrap;
}

.text-popap {
    font-size: 14px !important;
    text-align: justify !important;
    width: 280px;
    font-family: Arial;
    color: black;
    margin: 0px;
    padding: 0px;
}

.field-popap {
    font-size: 16px !important;
    text-align: left !important;
    width: 280px;
    font-family: Arial;
    color: black;
    margin: 0px;
    padding: 0px;
    background-color: #ebecec;
}

.but-popap {
    font-size: 16px !important;
    text-align: left !important;
    width: 278px;
    font-family: Arial;
    color: black;
    margin: 0px;
    padding: 0px;
    background-color: #960e0e;
    cursor: pointer;
}

.error-popap {
    font-size: 16px !important;
    text-align: left !important;
    width: 280px;
    height: 35px;
    font-family: Arial;
    color: black;
    margin: 0px;
    padding: 0px;
    background-color: #daabab;
    padding-left: 10px;
}

.active-field-popap {
    border: 1px solid #f2c3cb;
    width: 268px !important;
    height: 33px !important;
}

.field-popap:focus {
    background-color: black;
}

.box{
    position:fixed;
    top:-200px;
    background-color: #fff;
    z-index: 101;
    width: 320px;
    padding: 0px;
    margin: 0px;
    left: 40%;
    display: none;
}

a.boxclose{
    float: right;
    width: 22px;
    height: 22px;
    background: transparent url(images/cancel.png) repeat top left;
    margin-top: -13px;
    margin-right: -12px;
    cursor: pointer;
}

input[name="email_address"]::-webkit-input-placeholder {
    color: black;
} 

input[name="email_address"]::-moz-placeholder {
    color: black;
}

input[name="email_address"]:focus {
    outline: none;
}

input[name="phone_number"]::-webkit-input-placeholder {
    color: black;
} 
input[name="phone_number"]::-moz-placeholder {
    color: black;
}

input[name="phone_number"]:focus {
    outline: none;
}

.box_field {
    color: black;
    border: none;
    height: 35px;
    width: 270px;
    padding: 0px;
    margin: 0px;
    background-color: #ebecec;
    margin-left: 10px;
}

.active_box_field {
    width: 268px !important;
    height: 33px !important;
}

.box_request {
    background-color: #960e0e;
    color: white;
    padding: 0px;
    margin: 0px;
    left: 40%;
    font-weight: bold;
    padding-top: 8px;
    padding-left: 10px;
    height: 27px;
}

</style>
{/literal}

{literal}
<script language="javascript">
function fn_open_click_box(product)
{
	$('#overlay').fadeIn('fast',function(){
        $('.error-popap').hide();
        $('.td-error-popap').hide();
        $('#box_{/literal}{$obj_id}{literal}').show();
        $('#box_{/literal}{$obj_id}{literal}').animate({'top':'160px'},500);
    });
}

$('#boxclose').click(function(){
    $('#box_{/literal}{$obj_id}{literal}').animate({'top':'-200px'},500,function(){
        $('#overlay').fadeOut('fast');
        $('#box_{/literal}{$obj_id}{literal}').hide();
        $('#form-request-callback_{/literal}{$obj_id}{literal}').show();
        $('#form-thanks_{/literal}{$obj_id}{literal}').hide();
    });
});

function fn_request_callback()
{
	email = $('#email_address_{/literal}{$obj_id}{literal}').val();
	phone = $('#phone_number_{/literal}{$obj_id}{literal}').val();
    $('.error-popap').hide();
    $('.td-error-popap').hide();
    
    $('#phone_{/literal}{$obj_id}{literal}').removeClass("active-field-popap");
    $('#email_{/literal}{$obj_id}{literal}').removeClass("active-field-popap");
    
    $('#phone_number_{/literal}{$obj_id}{literal}').removeClass("active_box_field");
    $('#email_address_{/literal}{$obj_id}{literal}').removeClass("active_box_field");
    
    if (fn_chech_box_fields()) {
        jQuery.ajaxRequest('{/literal}{"checkout.request_callback&product_id=`$obj_id`"|fn_url}{literal}&email=' + email + '&phone=' + phone);
        $('#form-request-callback_{/literal}{$obj_id}{literal}').hide();
        $('#form-thanks_{/literal}{$obj_id}{literal}').show();
        setTimeout(function() {
        	$('#box_{/literal}{$obj_id}{literal}').animate({'top':'-200px'},500,function(){
	        $('#overlay').fadeOut('fast');
	        $('#box_{/literal}{$obj_id}{literal}').hide();
	        $('#form-request-callback_{/literal}{$obj_id}{literal}').show();
	        $('#form-thanks_{/literal}{$obj_id}{literal}').hide();
    	});

    	}, 3000);
    }
}

function fn_chech_box_fields()
{
    $('.field-popap').removeClass('active-field-popap');
    $('.box_field').removeClass('active_box_field');
    
    if ($('#email_address_{/literal}{$obj_id}{literal}').val() == '') {
        $('.error-popap').show();
        $('.td-error-popap').show();
        $('#email_{/literal}{$obj_id}{literal}').addClass("active-field-popap");
        $('#email_address_{/literal}{$obj_id}{literal}').addClass("active_box_field");
        return false;
    }
    if (!fn_is_valid_email($('#email_address_{/literal}{$obj_id}{literal}').val())) {
    	$('.error-popap').show();
        $('.td-error-popap').show();
        $('#email_{/literal}{$obj_id}{literal}').addClass("active-field-popap");
        $('#email_address_{/literal}{$obj_id}{literal}').addClass("active_box_field");
        return false;
    }
    if ($('#phone_number_{/literal}{$obj_id}{literal}').val() == '') {
        $('.error-popap').show();
        $('.td-error-popap').show();
        $('#phone_{/literal}{$obj_id}{literal}').addClass("active-field-popap");
        $('#phone_number_{/literal}{$obj_id}{literal}').addClass("active_box_field");
        return false;
    }
    return true;
}

function fn_is_valid_email(email)
{
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    
    return pattern.test(email);
}

function fn_close_box()
{
	$('#overlay').fadeOut('fast');
    $('#box_{/literal}{$obj_id}{literal}').hide();
    $('#form-request-callback_{/literal}{$obj_id}{literal}').show();
    $('#form-thanks_{/literal}{$obj_id}{literal}').hide();
}

function fn_click_field_popap(el, elf)
{
    $('.field-popap').removeClass('active-field-popap');
    $('.box_field').removeClass('active_box_field');
    $('#' + el).addClass('active-field-popap');
    $('#' + elf).addClass('active_box_field');
}
</script>
{/literal}

<div class="overlay" id="overlay" style="display:none;"></div>
<div class="box" id="box_{$obj_id}" style="">
    <a class="boxclose" id="boxclose" onclick="fn_close_box();"></a>
    <table id="form-request-callback_{$obj_id}" class="table-request-callback">
        <tr>
        <td class="product-popap">{$product.product}</td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
        <td class="text-popap">{$lang.prev_text}</td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
        <td class="field-popap" id="email_{$obj_id}"><input class="box_field" name="email_address" id="email_address_{$obj_id}" placeholder="{$lang.email_address}" onfocus="fn_click_field_popap('email_{$obj_id}', 'email_address_{$obj_id}');"></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
        <td class="field-popap" id="phone_{$obj_id}"><input class="box_field" name="phone_number" id="phone_number_{$obj_id}" placeholder="{$lang.phone_number}" onclick="fn_click_field_popap('phone_{$obj_id}', 'phone_number_{$obj_id}');"></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
        <td class="error-popap">{$lang.enter_fields}</td>
        </tr>
        <tr><td class="td-error-popap">&nbsp;</td></tr>
        <tr>
        <td class="but-popap" onclick="fn_request_callback();" style="text-decoration: none;"><div class="box_request">&#9658;&nbsp;{$lang.request_callback}</div></td>
        </tr>

    </table>
    <table id="form-thanks_{$obj_id}" style="display: none;" class="table-request-callback">
        <tr>
        <td class="product-popap">{$lang.thank|unescape}</td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr>
        <td class="text-popap">{$lang.thank_message}</td>
        </tr>
    </table>
</div>

{else}

{if $product.has_options && !$show_product_options && !$details_page}
					{include file="buttons/button.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_text=$lang.select_options but_href="products.view?product_id=`$product.product_id`" but_role="text" but_name=""}
				{else}
					{if $extra_button}{$extra_button}&nbsp;{/if}
					{include file="buttons/add_to_cart.tpl" but_id="button_cart_`$obj_prefix``$obj_id`" but_name="dispatch[checkout.add..`$obj_id`]" but_role=$but_role block_width=$block_width obj_id=$obj_id product=$product}
					{assign var="cart_button_exists" value=true}
				{/if}

{/if}