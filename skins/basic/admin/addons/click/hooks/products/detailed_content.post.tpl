{* $Id$ *}

<fieldset>
	{include file="common_templates/subheader.tpl" title=$lang.click_collect_requirment}

	<div class="form-field id="click">
        <label for="click">{$lang.click_collect}:</label>
        <input type="hidden" name="product_data[requirement]" value="N" />
        <input type="checkbox" name="product_data[requirement]" id="click" value="Y" {if $product_data.requirement == "Y"}checked="checked"{/if} class="checkbox" />
    </div>
</fieldset>