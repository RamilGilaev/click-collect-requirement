DROP TABLE IF EXISTS cscart_access_restriction;
CREATE TABLE `cscart_access_restriction` (
  `item_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(66) NOT NULL DEFAULT '',
  `ip_from` int(11) unsigned NOT NULL DEFAULT '0',
  `ip_to` int(11) unsigned NOT NULL DEFAULT '0',
  `type` char(3) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_access_restriction_block;
CREATE TABLE `cscart_access_restriction_block` (
  `ip` int(11) unsigned NOT NULL DEFAULT '0',
  `tries` smallint(5) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `expires` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_access_restriction_reason_descriptions;
CREATE TABLE `cscart_access_restriction_reason_descriptions` (
  `item_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(3) NOT NULL DEFAULT '',
  `reason` mediumtext NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`item_id`,`type`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_addon_descriptions;
CREATE TABLE `cscart_addon_descriptions` (
  `addon` varchar(32) NOT NULL DEFAULT '',
  `object_id` varchar(64) NOT NULL DEFAULT '',
  `object_type` char(1) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `tooltip` text NOT NULL,
  `lang_code` varchar(2) NOT NULL DEFAULT '',
  PRIMARY KEY (`addon`,`object_id`,`object_type`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_addons;
CREATE TABLE `cscart_addons` (
  `addon` varchar(32) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `options` text NOT NULL,
  `priority` int(11) unsigned NOT NULL DEFAULT '0',
  `dependencies` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`addon`),
  KEY `priority` (`priority`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_aff_action_links;
CREATE TABLE `cscart_aff_action_links` (
  `action_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_data` varchar(255) NOT NULL DEFAULT '',
  `object_type` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`action_id`,`object_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_aff_banner_descriptions;
CREATE TABLE `cscart_aff_banner_descriptions` (
  `banner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `title` varchar(64) NOT NULL DEFAULT '',
  `content` varchar(255) NOT NULL DEFAULT '',
  `alt` varchar(64) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`banner_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_aff_banners;
CREATE TABLE `cscart_aff_banners` (
  `banner_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `width` int(4) unsigned NOT NULL DEFAULT '120',
  `height` int(4) unsigned NOT NULL DEFAULT '60',
  `type` char(1) NOT NULL DEFAULT 'T',
  `link_to` char(1) NOT NULL DEFAULT 'U',
  `data` varchar(255) NOT NULL DEFAULT '',
  `show_title` char(1) NOT NULL DEFAULT 'Y',
  `text_location` char(1) NOT NULL DEFAULT 'B',
  `new_window` char(1) NOT NULL DEFAULT 'N',
  `to_cart` char(1) NOT NULL DEFAULT 'N',
  `show_url` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`banner_id`),
  KEY `type_linkto` (`type`,`link_to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_aff_group_descriptions;
CREATE TABLE `cscart_aff_group_descriptions` (
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` char(64) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`group_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_aff_groups;
CREATE TABLE `cscart_aff_groups` (
  `group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `link_to` char(1) NOT NULL DEFAULT 'U',
  `data` char(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`group_id`),
  KEY `link_to` (`link_to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_aff_partner_actions;
CREATE TABLE `cscart_aff_partner_actions` (
  `action_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `banner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `partner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `plan_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `customer_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `ip` varchar(255) NOT NULL DEFAULT '',
  `action` varchar(32) NOT NULL DEFAULT '',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `approved` char(1) NOT NULL DEFAULT 'N',
  `payout_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`),
  KEY `parnerid_approved_date_payoutid` (`partner_id`,`approved`,`date`,`payout_id`,`amount`),
  KEY `action_date_amount_approved_payoutid` (`action`,`date`,`amount`,`approved`,`payout_id`),
  KEY `data_approved_payoutid` (`approved`,`payout_id`),
  KEY `amount` (`amount`),
  KEY `partnerid_date` (`partner_id`,`date`),
  KEY `payout_id` (`payout_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_aff_partner_profiles;
CREATE TABLE `cscart_aff_partner_profiles` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `approved` char(1) NOT NULL DEFAULT 'N',
  `plan_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `balance` decimal(12,2) NOT NULL DEFAULT '0.00',
  `referrer_partner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_affiliate_payouts;
CREATE TABLE `cscart_affiliate_payouts` (
  `payout_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `partner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'O',
  PRIMARY KEY (`payout_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_affiliate_plans;
CREATE TABLE `cscart_affiliate_plans` (
  `plan_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `payout_types` text NOT NULL,
  `commissions` varchar(255) NOT NULL DEFAULT '',
  `min_payment` decimal(12,2) NOT NULL DEFAULT '0.00',
  `product_ids` text NOT NULL,
  `category_ids` text NOT NULL,
  `promotion_ids` text NOT NULL,
  `cookie_expiration` int(11) unsigned NOT NULL DEFAULT '0',
  `method_based_selling_price` char(1) NOT NULL DEFAULT 'N',
  `use_coupon_commission` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_also_bought_products;
CREATE TABLE `cscart_also_bought_products` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `related_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`related_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_attachment_descriptions;
CREATE TABLE `cscart_attachment_descriptions` (
  `attachment_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`attachment_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_attachments;
CREATE TABLE `cscart_attachments` (
  `attachment_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `object_type` varchar(30) NOT NULL DEFAULT '',
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT '',
  `position` int(11) NOT NULL DEFAULT '0',
  `filename` varchar(100) NOT NULL DEFAULT '',
  `filesize` int(11) unsigned NOT NULL DEFAULT '0',
  `usergroup_ids` varchar(255) NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`attachment_id`),
  KEY `object_type` (`object_type`,`object_id`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_banner_descriptions;
CREATE TABLE `cscart_banner_descriptions` (
  `banner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `banner` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`banner_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_banners;
CREATE TABLE `cscart_banners` (
  `banner_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `type` char(1) NOT NULL DEFAULT 'G',
  `target` char(1) NOT NULL DEFAULT 'B',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`banner_id`),
  KEY `localization` (`localization`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_block_descriptions;
CREATE TABLE `cscart_block_descriptions` (
  `block_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_type` char(1) NOT NULL DEFAULT 'B',
  `object_text_id` varchar(32) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `lang_code` varchar(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`block_id`,`object_id`,`object_type`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_block_links;
CREATE TABLE `cscart_block_links` (
  `link_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `block_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `location` varchar(32) NOT NULL DEFAULT '',
  `object_id` int(11) unsigned NOT NULL DEFAULT '0',
  `item_ids` text NOT NULL,
  `enable` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `block_id` (`block_id`,`object_id`),
  KEY `block_id_2` (`block_id`,`enable`),
  KEY `loc` (`location`,`enable`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_block_location_descriptions;
CREATE TABLE `cscart_block_location_descriptions` (
  `location` varchar(32) NOT NULL DEFAULT '',
  `property` varchar(32) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `lang_code` varchar(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`location`,`property`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_block_location_properties;
CREATE TABLE `cscart_block_location_properties` (
  `location` varchar(32) NOT NULL DEFAULT '',
  `property` varchar(32) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`location`,`property`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_block_positions;
CREATE TABLE `cscart_block_positions` (
  `block_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `text_id` varchar(32) NOT NULL DEFAULT '',
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `location` varchar(32) NOT NULL DEFAULT '',
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`block_id`,`text_id`,`object_id`,`location`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_blocks;
CREATE TABLE `cscart_blocks` (
  `block_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `text_id` varchar(32) NOT NULL DEFAULT '',
  `block_type` char(1) NOT NULL DEFAULT 'B',
  `location` varchar(32) NOT NULL DEFAULT '',
  `disabled_locations` varchar(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `properties` text NOT NULL,
  `company_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`block_id`),
  KEY `disabled_locations` (`disabled_locations`),
  KEY `sloc` (`status`,`location`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_categories;
CREATE TABLE `cscart_categories` (
  `category_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id_path` varchar(255) NOT NULL DEFAULT '',
  `owner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `usergroup_ids` varchar(255) NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `product_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `is_op` char(1) NOT NULL DEFAULT 'N',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `age_verification` char(1) NOT NULL DEFAULT 'N',
  `age_limit` tinyint(4) NOT NULL DEFAULT '0',
  `parent_age_verification` char(1) NOT NULL DEFAULT 'N',
  `parent_age_limit` tinyint(4) NOT NULL DEFAULT '0',
  `selected_layouts` text NOT NULL,
  `default_layout` varchar(50) NOT NULL DEFAULT '',
  `product_details_layout` varchar(50) NOT NULL DEFAULT '',
  `product_columns` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  KEY `c_status` (`usergroup_ids`,`status`,`parent_id`),
  KEY `position` (`position`),
  KEY `parent` (`parent_id`),
  KEY `id_path` (`id_path`),
  KEY `localization` (`localization`),
  KEY `age_verification` (`age_verification`,`age_limit`),
  KEY `parent_age_verification` (`parent_age_verification`,`parent_age_limit`),
  KEY `p_category_id` (`category_id`,`usergroup_ids`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_category_descriptions;
CREATE TABLE `cscart_category_descriptions` (
  `category_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `category` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `meta_keywords` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `age_warning_message` text NOT NULL,
  PRIMARY KEY (`category_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_common_descriptions;
CREATE TABLE `cscart_common_descriptions` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_type` varchar(32) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `object` varchar(128) NOT NULL DEFAULT '',
  `object_holder` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`object_id`,`lang_code`,`object_holder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_companies;
CREATE TABLE `cscart_companies` (
  `company_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT 'A',
  `company` varchar(255) NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `address` varchar(255) NOT NULL,
  `city` varchar(64) NOT NULL,
  `state` varchar(32) NOT NULL,
  `country` char(2) NOT NULL,
  `zipcode` varchar(16) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `url` varchar(128) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `categories` text NOT NULL,
  `shippings` text NOT NULL,
  `logos` text NOT NULL,
  `commission` decimal(12,2) NOT NULL DEFAULT '0.00',
  `commission_type` char(1) NOT NULL DEFAULT 'A',
  `request_user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `request_account_name` varchar(255) NOT NULL DEFAULT '',
  `request_account_data` text NOT NULL,
  PRIMARY KEY (`company_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_class_descriptions;
CREATE TABLE `cscart_conf_class_descriptions` (
  `class_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `class_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`class_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_class_products;
CREATE TABLE `cscart_conf_class_products` (
  `class_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `class_id` (`class_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_classes;
CREATE TABLE `cscart_conf_classes` (
  `class_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT 'A',
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`class_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_compatible_classes;
CREATE TABLE `cscart_conf_compatible_classes` (
  `master_class_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `slave_class_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `master_class_id` (`master_class_id`,`slave_class_id`),
  KEY `slave_class_id` (`slave_class_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_group_descriptions;
CREATE TABLE `cscart_conf_group_descriptions` (
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `configurator_group_name` varchar(255) NOT NULL DEFAULT '',
  `full_description` mediumtext NOT NULL,
  PRIMARY KEY (`group_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_group_products;
CREATE TABLE `cscart_conf_group_products` (
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  KEY `group_id` (`group_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_groups;
CREATE TABLE `cscart_conf_groups` (
  `group_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `configurator_group_type` char(1) NOT NULL DEFAULT 'S',
  `status` char(1) NOT NULL DEFAULT 'A',
  `step_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`),
  KEY `step_id` (`step_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_product_groups;
CREATE TABLE `cscart_conf_product_groups` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `group_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `default_product_ids` text NOT NULL,
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'N',
  KEY `group_id` (`group_id`,`product_id`,`required`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_step_descriptions;
CREATE TABLE `cscart_conf_step_descriptions` (
  `step_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `step_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`step_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_conf_steps;
CREATE TABLE `cscart_conf_steps` (
  `step_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`step_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_countries;
CREATE TABLE `cscart_countries` (
  `code` char(2) NOT NULL DEFAULT '',
  `code_A3` char(3) NOT NULL DEFAULT '',
  `code_N3` char(3) NOT NULL DEFAULT '',
  `region` char(2) NOT NULL DEFAULT '',
  `lat` float NOT NULL DEFAULT '0',
  `lon` float NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`code`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_country_descriptions;
CREATE TABLE `cscart_country_descriptions` (
  `code` char(2) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `country` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`code`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_currencies;
CREATE TABLE `cscart_currencies` (
  `currency_code` varchar(10) NOT NULL DEFAULT '',
  `after` char(1) NOT NULL DEFAULT 'N',
  `symbol` varchar(30) NOT NULL DEFAULT '',
  `coefficient` float(12,5) NOT NULL DEFAULT '1.00000',
  `is_primary` char(1) NOT NULL DEFAULT 'N',
  `position` smallint(5) NOT NULL,
  `decimals_separator` char(1) NOT NULL DEFAULT '.',
  `thousands_separator` char(1) NOT NULL DEFAULT ',',
  `decimals` smallint(5) NOT NULL DEFAULT '2',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`currency_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_currency_descriptions;
CREATE TABLE `cscart_currency_descriptions` (
  `currency_code` varchar(10) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`currency_code`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_destination_descriptions;
CREATE TABLE `cscart_destination_descriptions` (
  `destination_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `destination` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`destination_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_destination_elements;
CREATE TABLE `cscart_destination_elements` (
  `element_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `destination_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element` varchar(36) NOT NULL DEFAULT '',
  `element_type` char(1) NOT NULL DEFAULT 'S',
  PRIMARY KEY (`element_id`),
  KEY `c_status` (`destination_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_destinations;
CREATE TABLE `cscart_destinations` (
  `destination_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `localization` varchar(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`destination_id`),
  KEY `localization` (`localization`),
  KEY `c_status` (`destination_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_discussion;
CREATE TABLE `cscart_discussion` (
  `thread_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_type` char(1) NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'D',
  PRIMARY KEY (`thread_id`),
  UNIQUE KEY `object_id` (`object_id`,`object_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_discussion_messages;
CREATE TABLE `cscart_discussion_messages` (
  `message` mediumtext NOT NULL,
  `post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `thread_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`),
  KEY `thread_id` (`thread_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_discussion_posts;
CREATE TABLE `cscart_discussion_posts` (
  `post_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `thread_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'D',
  PRIMARY KEY (`post_id`),
  KEY `thread_id` (`thread_id`,`ip_address`),
  KEY `thread_id_2` (`thread_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_discussion_rating;
CREATE TABLE `cscart_discussion_rating` (
  `rating_value` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `post_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `thread_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`post_id`),
  KEY `thread_id` (`thread_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_ekeys;
CREATE TABLE `cscart_ekeys` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_string` varchar(128) NOT NULL DEFAULT '',
  `object_type` char(1) NOT NULL DEFAULT 'R',
  `ekey` varchar(32) NOT NULL DEFAULT '',
  `ttl` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`object_type`,`ekey`),
  UNIQUE KEY `object_string` (`object_string`,`object_type`,`ekey`),
  KEY `c_status` (`ekey`,`object_type`,`ttl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_exim_layouts;
CREATE TABLE `cscart_exim_layouts` (
  `layout_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL DEFAULT '',
  `cols` text NOT NULL,
  `pattern_id` varchar(128) NOT NULL DEFAULT '',
  `active` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`layout_id`),
  KEY `pattern_id` (`pattern_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_form_descriptions;
CREATE TABLE `cscart_form_descriptions` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`object_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_form_options;
CREATE TABLE `cscart_form_options` (
  `element_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element_type` char(1) NOT NULL DEFAULT 'I',
  `value` varchar(255) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`element_id`),
  KEY `page_id` (`page_id`,`status`),
  KEY `position` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_gift_certificates;
CREATE TABLE `cscart_gift_certificates` (
  `gift_cert_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `gift_cert_code` varchar(255) NOT NULL DEFAULT '',
  `sender` varchar(64) NOT NULL DEFAULT '',
  `recipient` varchar(64) NOT NULL DEFAULT '',
  `send_via` char(1) NOT NULL DEFAULT 'E',
  `amount_type` char(1) NOT NULL DEFAULT 'I',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `email` varchar(64) NOT NULL DEFAULT '',
  `address` varchar(255) NOT NULL DEFAULT '',
  `address_2` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(255) NOT NULL DEFAULT '',
  `state` varchar(32) NOT NULL DEFAULT '',
  `country` char(2) NOT NULL DEFAULT '',
  `zipcode` varchar(10) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'P',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `phone` varchar(32) NOT NULL DEFAULT '',
  `order_ids` varchar(255) NOT NULL DEFAULT '',
  `template` varchar(128) NOT NULL DEFAULT '',
  `message` mediumtext NOT NULL,
  `products` text NOT NULL,
  PRIMARY KEY (`gift_cert_id`),
  KEY `status` (`status`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_gift_certificates_log;
CREATE TABLE `cscart_gift_certificates_log` (
  `log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `gift_cert_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `area` char(1) NOT NULL DEFAULT 'C',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `amount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `debit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `products` text NOT NULL,
  `debit_products` text NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `area` (`area`),
  KEY `user_id` (`user_id`),
  KEY `order_id` (`order_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_giftreg_descriptions;
CREATE TABLE `cscart_giftreg_descriptions` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `object_type` char(1) NOT NULL DEFAULT 'F',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`object_id`,`object_type`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_giftreg_event_fields;
CREATE TABLE `cscart_giftreg_event_fields` (
  `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `field_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `value` text NOT NULL,
  PRIMARY KEY (`event_id`,`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_giftreg_event_products;
CREATE TABLE `cscart_giftreg_event_products` (
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` smallint(5) unsigned NOT NULL DEFAULT '0',
  `ordered_amount` smallint(5) unsigned NOT NULL DEFAULT '0',
  `extra` text,
  PRIMARY KEY (`item_id`,`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_giftreg_event_subscribers;
CREATE TABLE `cscart_giftreg_event_subscribers` (
  `event_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`event_id`,`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_giftreg_events;
CREATE TABLE `cscart_giftreg_events` (
  `event_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `start_date` int(11) unsigned NOT NULL DEFAULT '0',
  `end_date` int(11) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `type` char(1) NOT NULL DEFAULT 'P',
  `title` varchar(255) NOT NULL DEFAULT '',
  `owner` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`event_id`),
  KEY `start_date` (`start_date`,`end_date`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_giftreg_field_variants;
CREATE TABLE `cscart_giftreg_field_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variant_id`),
  KEY `field_id` (`field_id`),
  KEY `position` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_giftreg_fields;
CREATE TABLE `cscart_giftreg_fields` (
  `field_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `field_type` char(1) NOT NULL DEFAULT 'I',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`field_id`),
  KEY `position` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_images;
CREATE TABLE `cscart_images` (
  `image_id` mediumint(8) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(255) NOT NULL DEFAULT '',
  `image_x` int(5) NOT NULL DEFAULT '0',
  `image_y` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`image_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_images_links;
CREATE TABLE `cscart_images_links` (
  `pair_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) unsigned NOT NULL DEFAULT '0',
  `object_type` varchar(24) NOT NULL DEFAULT '',
  `image_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `detailed_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT 'M',
  PRIMARY KEY (`pair_id`),
  KEY `object_id` (`object_id`,`object_type`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_language_values;
CREATE TABLE `cscart_language_values` (
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `name` varchar(128) NOT NULL DEFAULT '',
  `value` text NOT NULL,
  PRIMARY KEY (`lang_code`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_languages;
CREATE TABLE `cscart_languages` (
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `name` varchar(64) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_left_messages;
CREATE TABLE `cscart_lh_left_messages` (
  `message_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL DEFAULT '0',
  `reply_date` int(11) NOT NULL DEFAULT '0',
  `from_name` varchar(255) NOT NULL DEFAULT '',
  `from_mail` varchar(64) NOT NULL DEFAULT '',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` mediumtext NOT NULL,
  `operator_id` smallint(8) NOT NULL DEFAULT '0',
  `reply` text NOT NULL,
  `replied` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_messages;
CREATE TABLE `cscart_lh_messages` (
  `message_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) NOT NULL DEFAULT '0',
  `direction` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `from_id` varchar(255) NOT NULL DEFAULT '0',
  `to_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `text` mediumtext NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `from_id` (`from_id`),
  KEY `to_id` (`to_id`),
  KEY `direction` (`direction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_messages_map;
CREATE TABLE `cscart_lh_messages_map` (
  `self_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `to_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `new_messages` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `last_message` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `direction` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `to_id` (`to_id`),
  KEY `self_id` (`self_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_operators;
CREATE TABLE `cscart_lh_operators` (
  `operator_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `login` varchar(255) NOT NULL DEFAULT '',
  `password` varchar(255) NOT NULL DEFAULT '',
  `obsolete_time` int(11) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `register_time` int(11) unsigned NOT NULL DEFAULT '0',
  `type_notify` char(1) NOT NULL DEFAULT 'N',
  `last_message` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`operator_id`),
  KEY `obsolete_time` (`obsolete_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_sessions;
CREATE TABLE `cscart_lh_sessions` (
  `session_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `ip` double NOT NULL DEFAULT '0',
  `time_diff` bigint(11) unsigned NOT NULL DEFAULT '0',
  `start_time` int(11) unsigned NOT NULL DEFAULT '0',
  `obsolete_time` int(11) unsigned NOT NULL DEFAULT '0',
  `last_message` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `readdress` varchar(100) NOT NULL DEFAULT '0',
  `chat_counter` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`session_id`),
  KEY `obsolete_time` (`obsolete_time`),
  KEY `ip` (`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_stickers;
CREATE TABLE `cscart_lh_stickers` (
  `sticker_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `sticker` varchar(255) NOT NULL,
  `text` mediumtext NOT NULL,
  `viewed` varchar(255) NOT NULL,
  `closed` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`sticker_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_track;
CREATE TABLE `cscart_lh_track` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_type` char(1) NOT NULL,
  `avail` char(1) NOT NULL DEFAULT 'S',
  `track` text NOT NULL,
  `chat_track` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_type_notify;
CREATE TABLE `cscart_lh_type_notify` (
  `direction` smallint(5) unsigned NOT NULL DEFAULT '0',
  `self_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `to_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `flag` smallint(5) unsigned NOT NULL DEFAULT '0',
  KEY `self_id` (`self_id`),
  KEY `to_id` (`to_id`),
  KEY `type` (`direction`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_visitors;
CREATE TABLE `cscart_lh_visitors` (
  `visitor_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip` double NOT NULL DEFAULT '0',
  `start_time` int(11) unsigned NOT NULL DEFAULT '0',
  `obsolete_time` int(11) unsigned NOT NULL DEFAULT '0',
  `obsolete_time_chat` int(11) unsigned NOT NULL DEFAULT '0',
  `status` smallint(5) unsigned NOT NULL DEFAULT '0',
  `operator_id` mediumint(8) NOT NULL DEFAULT '0',
  `assigned_name` varchar(255) NOT NULL DEFAULT '',
  `chat_counter` smallint(5) unsigned NOT NULL DEFAULT '0',
  `last_message` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type_notify` char(1) NOT NULL DEFAULT 'N',
  `stat_track` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`visitor_id`),
  KEY `ip` (`ip`),
  KEY `obsolete_time` (`obsolete_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_visitors_log;
CREATE TABLE `cscart_lh_visitors_log` (
  `log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `event` smallint(5) unsigned NOT NULL DEFAULT '0',
  `operator_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `operator_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`log_id`),
  KEY `visitor_id` (`visitor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_lh_visitors_notes;
CREATE TABLE `cscart_lh_visitors_notes` (
  `note_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `visitor_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `operator_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `note` text NOT NULL,
  `operator_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`note_id`),
  KEY `visitor_id` (`visitor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_localization_descriptions;
CREATE TABLE `cscart_localization_descriptions` (
  `localization_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  KEY `localisation_id` (`localization_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_localization_elements;
CREATE TABLE `cscart_localization_elements` (
  `element_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `localization_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element` varchar(36) NOT NULL DEFAULT '',
  `element_type` char(1) NOT NULL DEFAULT 'S',
  `position` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`element_id`),
  KEY `c_avail` (`localization_id`),
  KEY `element` (`element`,`element_type`),
  KEY `position` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_localizations;
CREATE TABLE `cscart_localizations` (
  `localization_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `custom_weight_settings` char(1) NOT NULL DEFAULT 'Y',
  `weight_symbol` varchar(255) NOT NULL DEFAULT '',
  `weight_unit` decimal(12,2) NOT NULL DEFAULT '0.00',
  `is_default` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`localization_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_logs;
CREATE TABLE `cscart_logs` (
  `log_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `type` varchar(16) NOT NULL DEFAULT '',
  `event_type` char(1) NOT NULL DEFAULT 'N',
  `action` varchar(16) NOT NULL DEFAULT '',
  `object` char(1) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `backtrace` text NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `object` (`object`),
  KEY `type` (`type`,`action`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_mailing_lists;
CREATE TABLE `cscart_mailing_lists` (
  `list_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `from_email` varchar(64) NOT NULL DEFAULT '',
  `from_name` varchar(128) NOT NULL DEFAULT '',
  `reply_to` varchar(64) NOT NULL DEFAULT '',
  `show_on_checkout` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `show_on_registration` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `show_on_sidebar` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'D',
  `register_autoresponder` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_new_orders;
CREATE TABLE `cscart_new_orders` (
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_news;
CREATE TABLE `cscart_news` (
  `news_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `date` int(11) unsigned NOT NULL DEFAULT '0',
  `separate` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'D',
  `localization` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`news_id`),
  KEY `localization` (`localization`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_news_descriptions;
CREATE TABLE `cscart_news_descriptions` (
  `news_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `news` varchar(128) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`news_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_newsletter_campaigns;
CREATE TABLE `cscart_newsletter_campaigns` (
  `campaign_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'D',
  PRIMARY KEY (`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_newsletter_descriptions;
CREATE TABLE `cscart_newsletter_descriptions` (
  `newsletter_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `newsletter` varchar(255) NOT NULL DEFAULT '',
  `newsletter_multiple` text NOT NULL,
  `body_html` mediumtext NOT NULL,
  `body_txt` mediumtext NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`newsletter_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_newsletter_links;
CREATE TABLE `cscart_newsletter_links` (
  `link_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` mediumint(8) unsigned NOT NULL,
  `newsletter_id` mediumint(8) unsigned NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '',
  `clicks` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_newsletters;
CREATE TABLE `cscart_newsletters` (
  `newsletter_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sent_date` int(11) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `type` char(1) NOT NULL DEFAULT 'N',
  `mailing_lists` varchar(255) NOT NULL DEFAULT '',
  `users` text NOT NULL,
  `abandoned_type` varchar(10) NOT NULL DEFAULT 'both',
  `abandoned_days` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsletter_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_order_data;
CREATE TABLE `cscart_order_data` (
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`order_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_order_details;
CREATE TABLE `cscart_order_details` (
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_code` varchar(32) NOT NULL DEFAULT '',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `amount` smallint(5) unsigned NOT NULL DEFAULT '0',
  `extra` text NOT NULL,
  PRIMARY KEY (`item_id`,`order_id`),
  KEY `o_k` (`order_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_order_docs;
CREATE TABLE `cscart_order_docs` (
  `doc_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `type` char(1) NOT NULL DEFAULT 'I',
  `order_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`doc_id`,`type`),
  KEY `type` (`order_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_order_transactions;
CREATE TABLE `cscart_order_transactions` (
  `payment_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `transaction_id` varchar(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT '',
  `extra` text NOT NULL,
  PRIMARY KEY (`payment_id`,`transaction_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_orders;
CREATE TABLE `cscart_orders` (
  `order_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `is_parent_order` char(1) NOT NULL DEFAULT 'N',
  `parent_order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `total` decimal(12,2) NOT NULL DEFAULT '0.00',
  `subtotal` decimal(12,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `subtotal_discount` decimal(12,2) NOT NULL DEFAULT '0.00',
  `payment_surcharge` decimal(12,2) NOT NULL DEFAULT '0.00',
  `shipping_ids` varchar(255) NOT NULL DEFAULT '',
  `shipping_cost` decimal(12,2) NOT NULL DEFAULT '0.00',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'O',
  `notes` text NOT NULL,
  `details` text NOT NULL,
  `promotions` text NOT NULL,
  `promotion_ids` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(32) NOT NULL DEFAULT '',
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `b_title` varchar(32) NOT NULL DEFAULT '',
  `b_firstname` varchar(128) NOT NULL DEFAULT '',
  `b_lastname` varchar(128) NOT NULL DEFAULT '',
  `b_address` varchar(255) NOT NULL DEFAULT '',
  `b_address_2` varchar(255) NOT NULL DEFAULT '',
  `b_city` varchar(64) NOT NULL DEFAULT '',
  `b_county` varchar(32) NOT NULL DEFAULT '',
  `b_state` varchar(32) NOT NULL DEFAULT '',
  `b_country` char(2) NOT NULL DEFAULT '',
  `b_zipcode` varchar(32) NOT NULL DEFAULT '',
  `b_phone` varchar(32) NOT NULL DEFAULT '',
  `s_title` varchar(32) NOT NULL DEFAULT '',
  `s_firstname` varchar(128) NOT NULL DEFAULT '',
  `s_lastname` varchar(128) NOT NULL DEFAULT '',
  `s_address` varchar(255) NOT NULL DEFAULT '',
  `s_address_2` varchar(255) NOT NULL DEFAULT '',
  `s_city` varchar(64) NOT NULL DEFAULT '',
  `s_county` varchar(32) NOT NULL DEFAULT '',
  `s_state` varchar(32) NOT NULL DEFAULT '',
  `s_country` char(2) NOT NULL DEFAULT '',
  `s_zipcode` varchar(32) NOT NULL DEFAULT '',
  `s_phone` varchar(32) NOT NULL DEFAULT '',
  `phone` varchar(32) NOT NULL DEFAULT '',
  `fax` varchar(32) NOT NULL DEFAULT '',
  `url` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `payment_id` mediumint(8) NOT NULL DEFAULT '0',
  `tax_exempt` char(1) NOT NULL DEFAULT 'N',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `repaid` int(11) NOT NULL DEFAULT '0',
  `validation_code` varchar(20) NOT NULL DEFAULT '',
  `localization_id` mediumint(8) NOT NULL,
  PRIMARY KEY (`order_id`),
  KEY `timestamp` (`timestamp`),
  KEY `user_id` (`user_id`),
  KEY `promotion_ids` (`promotion_ids`),
  KEY `status` (`status`),
  KEY `shipping_ids` (`shipping_ids`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_page_descriptions;
CREATE TABLE `cscart_page_descriptions` (
  `page_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `page` varchar(255) DEFAULT '0',
  `description` mediumtext,
  `meta_keywords` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`page_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_pages;
CREATE TABLE `cscart_pages` (
  `page_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id_path` varchar(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `page_type` char(1) NOT NULL DEFAULT 'T',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  `usergroup_ids` varchar(255) NOT NULL DEFAULT '0',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `new_window` tinyint(3) NOT NULL DEFAULT '0',
  `related_ids` text,
  `use_avail_period` char(1) NOT NULL DEFAULT 'N',
  `avail_from_timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `avail_till_timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`),
  KEY `localization` (`localization`),
  KEY `parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_payment_descriptions;
CREATE TABLE `cscart_payment_descriptions` (
  `payment_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `payment` varchar(128) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `instructions` mediumtext NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`payment_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_payment_processors;
CREATE TABLE `cscart_payment_processors` (
  `processor_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `processor` varchar(255) NOT NULL DEFAULT '',
  `processor_script` varchar(255) NOT NULL DEFAULT '',
  `processor_template` varchar(255) NOT NULL DEFAULT '',
  `admin_template` varchar(255) NOT NULL DEFAULT '',
  `callback` char(1) NOT NULL DEFAULT 'N',
  `type` char(1) NOT NULL DEFAULT 'P',
  PRIMARY KEY (`processor_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_payments;
CREATE TABLE `cscart_payments` (
  `payment_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `usergroup_ids` varchar(255) NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `template` varchar(128) NOT NULL DEFAULT '',
  `processor_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `a_surcharge` decimal(13,3) NOT NULL DEFAULT '0.000',
  `p_surcharge` decimal(13,3) NOT NULL DEFAULT '0.000',
  `localization` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`payment_id`),
  KEY `c_status` (`usergroup_ids`,`status`),
  KEY `position` (`position`),
  KEY `localization` (`localization`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_poll_descriptions;
CREATE TABLE `cscart_poll_descriptions` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `page_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `type` char(1) NOT NULL DEFAULT 'P',
  `description` mediumtext NOT NULL,
  PRIMARY KEY (`object_id`,`lang_code`,`type`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_poll_items;
CREATE TABLE `cscart_poll_items` (
  `item_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT 'Q',
  `position` smallint(5) NOT NULL DEFAULT '0',
  `required` char(1) NOT NULL DEFAULT '',
  `page_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`),
  KEY `parent_id` (`parent_id`),
  KEY `type` (`type`),
  KEY `page_id` (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_polls;
CREATE TABLE `cscart_polls` (
  `page_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `start_date` int(11) unsigned NOT NULL DEFAULT '0',
  `end_date` int(11) unsigned NOT NULL DEFAULT '0',
  `show_results` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_polls_answers;
CREATE TABLE `cscart_polls_answers` (
  `answer_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `vote_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`answer_id`,`vote_id`,`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_polls_votes;
CREATE TABLE `cscart_polls_votes` (
  `vote_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(15) NOT NULL DEFAULT '',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `time` int(11) NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT '',
  PRIMARY KEY (`vote_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_privilege_descriptions;
CREATE TABLE `cscart_privilege_descriptions` (
  `privilege` varchar(32) NOT NULL DEFAULT '',
  `description` varchar(128) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `section_id` mediumint(8) NOT NULL,
  PRIMARY KEY (`privilege`,`lang_code`),
  KEY `section_id` (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_privilege_section_descriptions;
CREATE TABLE `cscart_privilege_section_descriptions` (
  `section_id` mediumint(8) NOT NULL,
  `description` varchar(64) NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`section_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_privileges;
CREATE TABLE `cscart_privileges` (
  `privilege` varchar(32) NOT NULL DEFAULT '',
  `is_default` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`privilege`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_descriptions;
CREATE TABLE `cscart_product_descriptions` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `product` varchar(255) NOT NULL DEFAULT '',
  `shortname` varchar(255) NOT NULL DEFAULT '',
  `short_description` mediumtext NOT NULL,
  `full_description` mediumtext NOT NULL,
  `meta_keywords` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `search_words` text NOT NULL,
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `age_warning_message` text NOT NULL,
  PRIMARY KEY (`product_id`,`lang_code`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_feature_variant_descriptions;
CREATE TABLE `cscart_product_feature_variant_descriptions` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `page_title` varchar(255) NOT NULL DEFAULT '',
  `meta_keywords` varchar(255) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`variant_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_feature_variants;
CREATE TABLE `cscart_product_feature_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variant_id`),
  KEY `feature_id` (`feature_id`),
  KEY `position` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_features;
CREATE TABLE `cscart_product_features` (
  `feature_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_type` char(1) NOT NULL DEFAULT 'T',
  `categories_path` text NOT NULL,
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `display_on_product` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `display_on_catalog` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comparison` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`feature_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_features_descriptions;
CREATE TABLE `cscart_product_features_descriptions` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `full_description` mediumtext NOT NULL,
  `prefix` varchar(128) NOT NULL DEFAULT '',
  `suffix` varchar(128) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`feature_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_features_values;
CREATE TABLE `cscart_product_features_values` (
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant_id` mediumint(8) unsigned DEFAULT NULL,
  `value` varchar(255) NOT NULL DEFAULT '',
  `value_int` int(11) unsigned DEFAULT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  KEY `fl` (`feature_id`,`lang_code`,`variant_id`,`value`,`value_int`),
  KEY `variant_id` (`variant_id`),
  KEY `lang_code` (`lang_code`),
  KEY `product_id` (`product_id`),
  KEY `fpl` (`feature_id`,`product_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_file_descriptions;
CREATE TABLE `cscart_product_file_descriptions` (
  `file_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `file_name` varchar(255) NOT NULL DEFAULT '',
  `license` text NOT NULL,
  `readme` text NOT NULL,
  PRIMARY KEY (`file_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_file_ekeys;
CREATE TABLE `cscart_product_file_ekeys` (
  `ekey` varchar(32) NOT NULL DEFAULT '',
  `file_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `downloads` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `active` char(1) NOT NULL DEFAULT 'N',
  `ttl` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`file_id`,`order_id`),
  UNIQUE KEY `ekey` (`ekey`),
  KEY `ttl` (`ttl`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_files;
CREATE TABLE `cscart_product_files` (
  `file_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `file_path` varchar(255) NOT NULL DEFAULT '',
  `preview_path` varchar(255) NOT NULL DEFAULT '',
  `file_size` int(11) unsigned NOT NULL DEFAULT '0',
  `preview_size` int(11) unsigned NOT NULL DEFAULT '0',
  `agreement` char(1) NOT NULL DEFAULT 'N',
  `max_downloads` smallint(5) unsigned NOT NULL DEFAULT '0',
  `total_downloads` smallint(5) unsigned NOT NULL DEFAULT '0',
  `activation_type` char(1) NOT NULL DEFAULT 'M',
  `position` smallint(5) NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`file_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_filter_descriptions;
CREATE TABLE `cscart_product_filter_descriptions` (
  `filter_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `filter` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`filter_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_filter_ranges;
CREATE TABLE `cscart_product_filter_ranges` (
  `range_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `filter_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `from` decimal(12,2) NOT NULL DEFAULT '0.00',
  `to` decimal(12,2) NOT NULL DEFAULT '0.00',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`range_id`),
  KEY `from` (`from`,`to`),
  KEY `filter_id` (`filter_id`),
  KEY `feature_id` (`feature_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_filter_ranges_descriptions;
CREATE TABLE `cscart_product_filter_ranges_descriptions` (
  `range_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `range_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`range_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_filters;
CREATE TABLE `cscart_product_filters` (
  `filter_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `categories_path` text NOT NULL,
  `feature_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `field_type` char(1) NOT NULL DEFAULT '',
  `show_on_home_page` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`filter_id`),
  KEY `feature_id` (`feature_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_global_option_links;
CREATE TABLE `cscart_product_global_option_links` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`option_id`,`product_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_option_variants;
CREATE TABLE `cscart_product_option_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `modifier` decimal(13,3) NOT NULL DEFAULT '0.000',
  `modifier_type` char(1) NOT NULL DEFAULT 'A',
  `weight_modifier` decimal(12,3) NOT NULL DEFAULT '0.000',
  `weight_modifier_type` char(1) NOT NULL DEFAULT 'A',
  `point_modifier` decimal(12,3) NOT NULL DEFAULT '0.000',
  `point_modifier_type` char(1) NOT NULL DEFAULT 'A',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`variant_id`),
  KEY `position` (`position`),
  KEY `status` (`status`),
  KEY `option_id` (`option_id`,`status`),
  KEY `option_id_2` (`option_id`,`variant_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_option_variants_descriptions;
CREATE TABLE `cscart_product_option_variants_descriptions` (
  `variant_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `variant_name` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`variant_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_options;
CREATE TABLE `cscart_product_options` (
  `option_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `option_type` char(1) NOT NULL DEFAULT 'S',
  `inventory` char(1) NOT NULL DEFAULT 'Y',
  `regexp` varchar(255) NOT NULL DEFAULT '',
  `required` char(1) NOT NULL DEFAULT 'N',
  `multiupload` char(1) NOT NULL DEFAULT 'N',
  `allowed_extensions` varchar(255) NOT NULL DEFAULT '',
  `max_file_size` int(11) NOT NULL DEFAULT '0',
  `missing_variants_handling` char(1) NOT NULL DEFAULT 'M',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`),
  KEY `c_status` (`product_id`,`status`),
  KEY `position` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_options_descriptions;
CREATE TABLE `cscart_product_options_descriptions` (
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `option_text` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `comment` varchar(255) NOT NULL DEFAULT '',
  `inner_hint` varchar(255) NOT NULL DEFAULT '',
  `incorrect_message` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`option_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_options_exceptions;
CREATE TABLE `cscart_product_options_exceptions` (
  `exception_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `combination` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`exception_id`),
  KEY `product` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_options_inventory;
CREATE TABLE `cscart_product_options_inventory` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_code` varchar(32) NOT NULL DEFAULT '',
  `combination_hash` int(11) unsigned NOT NULL DEFAULT '0',
  `combination` varchar(255) NOT NULL DEFAULT '',
  `amount` mediumint(8) NOT NULL DEFAULT '0',
  `temp` char(1) NOT NULL DEFAULT 'N',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`combination_hash`),
  KEY `pc` (`product_id`,`combination`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_point_prices;
CREATE TABLE `cscart_product_point_prices` (
  `point_price_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `point_price` int(11) unsigned NOT NULL DEFAULT '0',
  `lower_limit` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`point_price_id`),
  UNIQUE KEY `unique_key` (`lower_limit`,`usergroup_id`,`product_id`),
  KEY `src_k` (`product_id`,`lower_limit`,`usergroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_popularity;
CREATE TABLE `cscart_product_popularity` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `viewed` int(11) NOT NULL DEFAULT '0',
  `added` int(11) NOT NULL DEFAULT '0',
  `deleted` int(11) NOT NULL DEFAULT '0',
  `bought` int(11) NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `total` (`product_id`,`total`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_prices;
CREATE TABLE `cscart_product_prices` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `lower_limit` smallint(5) unsigned NOT NULL DEFAULT '0',
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `usergroup` (`product_id`,`usergroup_id`,`lower_limit`),
  KEY `product_id` (`product_id`),
  KEY `lower_limit` (`lower_limit`),
  KEY `usergroup_id` (`usergroup_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_required_products;
CREATE TABLE `cscart_product_required_products` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `required_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`,`required_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_sales;
CREATE TABLE `cscart_product_sales` (
  `category_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `pa` (`product_id`,`amount`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_product_subscriptions;
CREATE TABLE `cscart_product_subscriptions` (
  `subscription_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `email` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`subscription_id`),
  UNIQUE KEY `pe` (`product_id`,`email`),
  KEY `pd` (`product_id`,`user_id`,`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_products;
CREATE TABLE `cscart_products` (
  `product_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `product_code` varchar(32) NOT NULL DEFAULT '',
  `product_type` char(1) NOT NULL DEFAULT 'P',
  `owner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `list_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `amount` mediumint(8) NOT NULL DEFAULT '0',
  `weight` decimal(12,2) NOT NULL DEFAULT '0.00',
  `length` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `width` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `height` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `shipping_freight` decimal(12,2) NOT NULL DEFAULT '0.00',
  `low_avail_limit` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `usergroup_ids` varchar(255) NOT NULL DEFAULT '0',
  `is_edp` char(1) NOT NULL DEFAULT 'N',
  `edp_shipping` char(1) NOT NULL DEFAULT 'N',
  `unlimited_download` char(1) NOT NULL DEFAULT 'N',
  `tracking` char(1) NOT NULL DEFAULT 'B',
  `free_shipping` char(1) NOT NULL DEFAULT 'N',
  `feature_comparison` char(1) NOT NULL DEFAULT 'N',
  `zero_price_action` char(1) NOT NULL DEFAULT 'R',
  `is_pbp` char(1) NOT NULL DEFAULT 'N',
  `is_op` char(1) NOT NULL DEFAULT 'N',
  `is_oper` char(1) NOT NULL DEFAULT 'N',
  `is_returnable` char(1) NOT NULL DEFAULT 'Y',
  `return_period` int(11) unsigned NOT NULL DEFAULT '10',
  `avail_since` int(11) unsigned NOT NULL DEFAULT '0',
  `out_of_stock_actions` char(1) NOT NULL DEFAULT 'N',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `min_qty` smallint(5) NOT NULL DEFAULT '0',
  `max_qty` smallint(5) NOT NULL DEFAULT '0',
  `qty_step` smallint(5) NOT NULL DEFAULT '0',
  `list_qty_count` smallint(5) NOT NULL DEFAULT '0',
  `tax_ids` varchar(255) NOT NULL DEFAULT '',
  `age_verification` char(1) NOT NULL DEFAULT 'N',
  `age_limit` tinyint(4) NOT NULL DEFAULT '0',
  `options_type` char(1) NOT NULL DEFAULT 'P',
  `exceptions_type` char(1) NOT NULL DEFAULT 'F',
  `details_layout` varchar(50) NOT NULL DEFAULT '',
  `shipping_params` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`product_id`),
  KEY `age_verification` (`age_verification`,`age_limit`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_products_categories;
CREATE TABLE `cscart_products_categories` (
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `category_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `link_type` char(1) NOT NULL DEFAULT 'M',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`,`product_id`),
  KEY `link_type` (`link_type`),
  KEY `pt` (`product_id`,`link_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_profile_field_descriptions;
CREATE TABLE `cscart_profile_field_descriptions` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `object_type` char(1) NOT NULL DEFAULT 'F',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`object_id`,`object_type`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_profile_field_values;
CREATE TABLE `cscart_profile_field_values` (
  `value_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `field_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`value_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_profile_fields;
CREATE TABLE `cscart_profile_fields` (
  `field_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `field_name` varchar(32) NOT NULL DEFAULT '',
  `profile_show` char(1) DEFAULT 'N',
  `profile_required` char(1) DEFAULT 'N',
  `checkout_show` char(1) DEFAULT 'N',
  `checkout_required` char(1) DEFAULT 'N',
  `partner_show` char(1) DEFAULT 'N',
  `partner_required` char(1) DEFAULT 'N',
  `field_type` char(1) NOT NULL DEFAULT 'I',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_default` char(1) DEFAULT 'N',
  `section` char(1) DEFAULT 'C',
  `matching_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`field_id`),
  KEY `field_name` (`field_name`),
  KEY `checkout_show` (`checkout_show`,`field_type`),
  KEY `profile_show` (`profile_show`,`field_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_profile_fields_data;
CREATE TABLE `cscart_profile_fields_data` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_type` char(1) NOT NULL DEFAULT 'U',
  `field_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`object_type`,`field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_promotion_descriptions;
CREATE TABLE `cscart_promotion_descriptions` (
  `promotion_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `short_description` text NOT NULL,
  `detailed_description` mediumtext NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`promotion_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_promotions;
CREATE TABLE `cscart_promotions` (
  `promotion_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `conditions` text NOT NULL,
  `bonuses` text NOT NULL,
  `to_date` int(11) unsigned NOT NULL DEFAULT '0',
  `from_date` int(11) unsigned NOT NULL DEFAULT '0',
  `priority` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `stop` char(1) NOT NULL DEFAULT 'N',
  `zone` enum('cart','catalog') NOT NULL DEFAULT 'catalog',
  `conditions_hash` text NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  `number_of_usages` mediumint(8) NOT NULL DEFAULT '0',
  `users_conditions_hash` text NOT NULL,
  PRIMARY KEY (`promotion_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_quick_menu;
CREATE TABLE `cscart_quick_menu` (
  `menu_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  `parent_id` mediumint(8) unsigned NOT NULL,
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_recurring_events;
CREATE TABLE `cscart_recurring_events` (
  `event_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `subscription_id` mediumint(8) unsigned NOT NULL,
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `event_type` char(1) NOT NULL DEFAULT 'C',
  PRIMARY KEY (`event_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_recurring_plans;
CREATE TABLE `cscart_recurring_plans` (
  `plan_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `price` text NOT NULL,
  `product_ids` text NOT NULL,
  `period` char(1) NOT NULL DEFAULT 'A',
  `by_period` mediumint(8) NOT NULL,
  `pay_day` mediumint(8) NOT NULL,
  `duration` mediumint(8) NOT NULL,
  `start_price` text NOT NULL,
  `start_duration` mediumint(8) NOT NULL,
  `start_duration_type` char(1) NOT NULL DEFAULT 'D',
  `allow_change_duration` char(1) NOT NULL DEFAULT 'N',
  `allow_unsubscribe` char(1) NOT NULL DEFAULT 'N',
  `allow_free_buy` char(1) NOT NULL DEFAULT 'N',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`plan_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_recurring_subscriptions;
CREATE TABLE `cscart_recurring_subscriptions` (
  `subscription_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` mediumint(8) unsigned NOT NULL,
  `plan_id` mediumint(8) unsigned NOT NULL,
  `user_id` mediumint(8) unsigned NOT NULL,
  `firstname` varchar(32) NOT NULL DEFAULT '',
  `lastname` varchar(32) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `last_timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `end_timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `order_ids` text NOT NULL,
  `product_ids` text NOT NULL,
  `start_price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `duration` mediumint(8) NOT NULL,
  `orig_duration` mediumint(8) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`subscription_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_reward_point_changes;
CREATE TABLE `cscart_reward_point_changes` (
  `change_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` int(11) NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `action` char(1) NOT NULL DEFAULT 'A',
  `reason` text NOT NULL,
  PRIMARY KEY (`change_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_reward_points;
CREATE TABLE `cscart_reward_points` (
  `reward_point_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` int(11) unsigned NOT NULL DEFAULT '0',
  `amount_type` char(1) NOT NULL DEFAULT 'A',
  `object_type` char(1) NOT NULL DEFAULT 'P',
  PRIMARY KEY (`reward_point_id`),
  UNIQUE KEY `unique_key` (`object_id`,`usergroup_id`,`object_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_rma_properties;
CREATE TABLE `cscart_rma_properties` (
  `property_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'R',
  `update_totals_and_inventory` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`property_id`),
  KEY `c_status` (`property_id`,`status`),
  KEY `status` (`status`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_rma_property_descriptions;
CREATE TABLE `cscart_rma_property_descriptions` (
  `property_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `property` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`property_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_rma_return_products;
CREATE TABLE `cscart_rma_return_products` (
  `return_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `reason` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT 'A',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `product_options` text,
  `product` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`return_id`,`item_id`,`type`),
  KEY `reason` (`reason`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_rma_returns;
CREATE TABLE `cscart_rma_returns` (
  `return_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `action` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'O',
  `total_amount` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `comment` text,
  `extra` text,
  PRIMARY KEY (`return_id`),
  KEY `order_id` (`order_id`),
  KEY `timestamp` (`timestamp`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports;
CREATE TABLE `cscart_sales_reports` (
  `report_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `type` char(1) NOT NULL DEFAULT '',
  `period` char(2) NOT NULL DEFAULT 'A',
  `time_from` int(11) NOT NULL DEFAULT '0',
  `time_to` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`report_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_descriptions;
CREATE TABLE `cscart_sales_reports_descriptions` (
  `report_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`report_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_elements;
CREATE TABLE `cscart_sales_reports_elements` (
  `element_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(66) NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'O',
  `depend_on_it` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_intervals;
CREATE TABLE `cscart_sales_reports_intervals` (
  `interval_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `value` int(11) unsigned NOT NULL DEFAULT '0',
  `interval_code` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`interval_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_table_conditions;
CREATE TABLE `cscart_sales_reports_table_conditions` (
  `table_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `code` varchar(64) NOT NULL DEFAULT '0',
  `sub_element_id` varchar(16) NOT NULL DEFAULT '0',
  PRIMARY KEY (`table_id`,`code`,`sub_element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_table_descriptions;
CREATE TABLE `cscart_sales_reports_table_descriptions` (
  `table_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`table_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_table_element_conditions;
CREATE TABLE `cscart_sales_reports_table_element_conditions` (
  `table_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element_hash` varchar(32) NOT NULL DEFAULT '',
  `element_code` varchar(64) NOT NULL DEFAULT '',
  `ids` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`table_id`,`element_hash`,`ids`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_table_elements;
CREATE TABLE `cscart_sales_reports_table_elements` (
  `report_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `table_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `element_hash` int(11) NOT NULL DEFAULT '0',
  `color` varchar(64) NOT NULL DEFAULT 'blueviolet',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `dependence` varchar(64) NOT NULL DEFAULT 'max_p',
  `limit_auto` mediumint(8) unsigned NOT NULL DEFAULT '5',
  PRIMARY KEY (`report_id`,`table_id`,`element_hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sales_reports_tables;
CREATE TABLE `cscart_sales_reports_tables` (
  `table_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `report_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT 'T',
  `display` varchar(64) NOT NULL DEFAULT 'order_amount',
  `interval_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `auto` char(1) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`table_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_seo_names;
CREATE TABLE `cscart_seo_names` (
  `name` varchar(250) NOT NULL DEFAULT '',
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT '',
  `dispatch` varchar(64) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`object_id`,`type`,`dispatch`,`lang_code`),
  KEY `name` (`name`,`lang_code`),
  KEY `type` (`name`,`type`,`lang_code`),
  KEY `dispatch` (`dispatch`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sessions;
CREATE TABLE `cscart_sessions` (
  `session_id` varchar(32) NOT NULL DEFAULT '',
  `expiry` int(11) unsigned NOT NULL DEFAULT '0',
  `data` mediumtext,
  `area` char(1) NOT NULL DEFAULT 'C',
  PRIMARY KEY (`session_id`,`area`),
  KEY `src` (`session_id`,`expiry`),
  KEY `expiry` (`expiry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_settings;
CREATE TABLE `cscart_settings` (
  `option_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL DEFAULT '',
  `section_id` varchar(64) NOT NULL DEFAULT '',
  `subsection_id` varchar(64) NOT NULL DEFAULT '',
  `option_type` char(1) NOT NULL DEFAULT 'I',
  `value` varchar(255) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `is_global` char(1) NOT NULL DEFAULT 'Y',
  `edition_type` set('NONE','ROOT','VENDOR','PRO:NONE','PRO:ROOT','MVE:NONE','MVE:ROOT','MSE:NONE','MSE:ROOT','MSE:VENDOR','MSE:VENDORONLY') NOT NULL DEFAULT 'ROOT',
  PRIMARY KEY (`option_id`),
  KEY `is_global` (`is_global`),
  KEY `position` (`position`),
  KEY `section_id` (`section_id`,`subsection_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_settings_descriptions;
CREATE TABLE `cscart_settings_descriptions` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `description` varchar(255) NOT NULL DEFAULT '',
  `object_type` char(1) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `object_string_id` varchar(64) NOT NULL DEFAULT '',
  `tooltip` text NOT NULL,
  PRIMARY KEY (`object_id`,`object_string_id`,`lang_code`,`object_type`),
  KEY `object_id` (`object_id`,`object_type`,`lang_code`),
  KEY `object_string_id` (`object_string_id`,`object_type`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_settings_elements;
CREATE TABLE `cscart_settings_elements` (
  `element_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `section_id` varchar(32) NOT NULL DEFAULT '',
  `subsection_id` varchar(32) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `element_type` char(1) NOT NULL DEFAULT '',
  `handler` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`element_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_settings_sections;
CREATE TABLE `cscart_settings_sections` (
  `section_id` varchar(32) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_settings_subsections;
CREATE TABLE `cscart_settings_subsections` (
  `subsection_id` varchar(32) NOT NULL DEFAULT '',
  `section_id` varchar(32) NOT NULL DEFAULT '',
  `position` smallint(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`subsection_id`,`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_settings_variants;
CREATE TABLE `cscart_settings_variants` (
  `variant_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `option_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `variant_name` varchar(64) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`variant_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_shipment_items;
CREATE TABLE `cscart_shipment_items` (
  `item_id` int(11) unsigned NOT NULL,
  `shipment_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `order_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_id`,`shipment_id`),
  KEY `shipment_id` (`shipment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_shipments;
CREATE TABLE `cscart_shipments` (
  `shipment_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `tracking_number` varchar(255) NOT NULL DEFAULT '',
  `carrier` varchar(255) NOT NULL DEFAULT '',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `comments` mediumtext NOT NULL,
  PRIMARY KEY (`shipment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_shipping_descriptions;
CREATE TABLE `cscart_shipping_descriptions` (
  `shipping_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `shipping` varchar(255) NOT NULL DEFAULT '',
  `delivery_time` varchar(64) NOT NULL DEFAULT '',
  PRIMARY KEY (`shipping_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_shipping_rates;
CREATE TABLE `cscart_shipping_rates` (
  `rate_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `shipping_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `destination_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `rate_value` text NOT NULL,
  PRIMARY KEY (`rate_id`),
  UNIQUE KEY `shipping_rate` (`shipping_id`,`destination_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_shipping_service_descriptions;
CREATE TABLE `cscart_shipping_service_descriptions` (
  `service_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL DEFAULT '',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`service_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_shipping_services;
CREATE TABLE `cscart_shipping_services` (
  `service_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `intershipper_code` char(3) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `carrier` varchar(10) NOT NULL DEFAULT '',
  `module` varchar(32) NOT NULL DEFAULT '',
  `code` varchar(64) NOT NULL DEFAULT '',
  `sp_file` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`service_id`),
  KEY `sa` (`service_id`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_shippings;
CREATE TABLE `cscart_shippings` (
  `shipping_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `destination` char(1) NOT NULL DEFAULT 'I',
  `min_weight` decimal(12,2) NOT NULL DEFAULT '0.00',
  `max_weight` decimal(12,2) NOT NULL DEFAULT '0.00',
  `usergroup_ids` varchar(255) NOT NULL DEFAULT '0',
  `rate_calculation` char(1) NOT NULL DEFAULT 'M',
  `service_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `tax_ids` varchar(255) NOT NULL DEFAULT '',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'D',
  `params` text NOT NULL,
  UNIQUE KEY `shipping_id` (`shipping_id`),
  KEY `position` (`position`),
  KEY `localization` (`localization`),
  KEY `c_status` (`usergroup_ids`,`min_weight`,`max_weight`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sitemap_descriptions;
CREATE TABLE `cscart_sitemap_descriptions` (
  `object_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `object_name` varchar(255) NOT NULL DEFAULT '',
  `object_description` text NOT NULL,
  `object_type` char(1) NOT NULL DEFAULT 'S',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`object_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sitemap_links;
CREATE TABLE `cscart_sitemap_links` (
  `link_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `link_href` varchar(255) NOT NULL DEFAULT '',
  `section_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `link_type` varchar(255) NOT NULL DEFAULT '',
  `company_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`link_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_sitemap_sections;
CREATE TABLE `cscart_sitemap_sections` (
  `section_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT 'A',
  `section_type` varchar(255) NOT NULL DEFAULT '1',
  `position` smallint(5) unsigned NOT NULL DEFAULT '0',
  `company_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`section_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_banners_log;
CREATE TABLE `cscart_stat_banners_log` (
  `banner_id` mediumint(8) NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT 'C',
  `timestamp` int(11) NOT NULL DEFAULT '0',
  KEY `banner_id` (`banner_id`,`type`,`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_browsers;
CREATE TABLE `cscart_stat_browsers` (
  `browser_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `browser` varchar(50) NOT NULL DEFAULT '',
  `version` varchar(10) NOT NULL DEFAULT '',
  PRIMARY KEY (`browser_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_ips;
CREATE TABLE `cscart_stat_ips` (
  `ip_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ip` int(11) unsigned NOT NULL DEFAULT '0',
  `country_code` char(2) NOT NULL DEFAULT '',
  `city` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`ip_id`),
  KEY `country_code` (`country_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_languages;
CREATE TABLE `cscart_stat_languages` (
  `lang_code` varchar(5) NOT NULL DEFAULT '',
  `language` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_product_search;
CREATE TABLE `cscart_stat_product_search` (
  `sess_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `search_string` text NOT NULL,
  `md5` varchar(32) NOT NULL DEFAULT '',
  `quantity` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sess_id`,`md5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_requests;
CREATE TABLE `cscart_stat_requests` (
  `req_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `url` text NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `https` char(1) NOT NULL DEFAULT 'N',
  `loadtime` int(11) unsigned NOT NULL DEFAULT '0',
  `sess_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `request_type` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`req_id`),
  KEY `sess_id` (`sess_id`),
  KEY `request_type` (`request_type`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_search_engines;
CREATE TABLE `cscart_stat_search_engines` (
  `engine_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `engine` varchar(128) NOT NULL DEFAULT '',
  PRIMARY KEY (`engine_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_search_phrases;
CREATE TABLE `cscart_stat_search_phrases` (
  `phrase_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `phrase` text NOT NULL,
  PRIMARY KEY (`phrase_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stat_sessions;
CREATE TABLE `cscart_stat_sessions` (
  `sess_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `os` varchar(30) NOT NULL DEFAULT '',
  `client_type` char(1) NOT NULL DEFAULT 'U',
  `browser_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `robot` varchar(64) NOT NULL DEFAULT '',
  `user_agent` varchar(255) NOT NULL DEFAULT '',
  `screen_x` smallint(5) unsigned NOT NULL DEFAULT '0',
  `screen_y` smallint(5) unsigned NOT NULL DEFAULT '0',
  `color` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_language` varchar(5) NOT NULL DEFAULT '',
  `session` varchar(32) NOT NULL DEFAULT '',
  `host_ip` int(11) unsigned NOT NULL DEFAULT '0',
  `proxy_ip` int(11) unsigned NOT NULL DEFAULT '0',
  `ip_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `uniq_code` int(11) unsigned NOT NULL DEFAULT '0',
  `referrer` text NOT NULL,
  `referrer_scheme` varchar(32) NOT NULL DEFAULT '',
  `referrer_host` varchar(128) NOT NULL DEFAULT '',
  `engine_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `phrase_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `expiry` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`sess_id`),
  KEY `session` (`session`,`expiry`),
  KEY `browser_id` (`browser_id`),
  KEY `ip_id` (`ip_id`),
  KEY `engine_id` (`engine_id`),
  KEY `phrase_id` (`phrase_id`),
  KEY `robot` (`robot`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_state_descriptions;
CREATE TABLE `cscart_state_descriptions` (
  `state_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` varchar(2) NOT NULL DEFAULT 'EN',
  `state` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`state_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_states;
CREATE TABLE `cscart_states` (
  `state_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `code` varchar(32) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`state_id`),
  UNIQUE KEY `cs` (`country_code`,`code`),
  KEY `code` (`code`),
  KEY `country_code` (`country_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_static_data;
CREATE TABLE `cscart_static_data` (
  `param_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `param` varchar(255) NOT NULL DEFAULT '',
  `param_2` varchar(255) NOT NULL DEFAULT '',
  `param_3` varchar(255) NOT NULL DEFAULT '',
  `param_4` varchar(255) NOT NULL DEFAULT '',
  `param_5` varchar(255) NOT NULL DEFAULT '',
  `section` char(1) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  `position` smallint(5) NOT NULL DEFAULT '0',
  `parent_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `id_path` varchar(255) NOT NULL DEFAULT '',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `company_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`param_id`),
  KEY `section` (`section`,`status`,`localization`),
  KEY `position` (`position`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_static_data_descriptions;
CREATE TABLE `cscart_static_data_descriptions` (
  `param_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` varchar(2) NOT NULL DEFAULT 'EN',
  `descr` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`param_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_status_data;
CREATE TABLE `cscart_status_data` (
  `status` char(1) NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'O',
  `param` char(255) NOT NULL DEFAULT '',
  `value` char(255) NOT NULL DEFAULT 'Y',
  PRIMARY KEY (`status`,`type`,`param`),
  KEY `inventory` (`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_status_descriptions;
CREATE TABLE `cscart_status_descriptions` (
  `status` char(1) NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'O',
  `description` varchar(255) NOT NULL DEFAULT '',
  `email_subj` varchar(255) NOT NULL DEFAULT '',
  `email_header` text NOT NULL,
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  PRIMARY KEY (`status`,`type`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_statuses;
CREATE TABLE `cscart_statuses` (
  `status` char(1) NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'O',
  `is_default` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`status`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_storage_data;
CREATE TABLE `cscart_storage_data` (
  `data_key` varchar(255) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`data_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_store_location_descriptions;
CREATE TABLE `cscart_store_location_descriptions` (
  `store_location_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT '',
  `name` varchar(80) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `city` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`store_location_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_store_locations;
CREATE TABLE `cscart_store_locations` (
  `store_location_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `position` smallint(5) NOT NULL DEFAULT '0',
  `country` char(2) NOT NULL DEFAULT '',
  `latitude` double NOT NULL DEFAULT '0',
  `longitude` double NOT NULL DEFAULT '0',
  `localization` varchar(255) NOT NULL DEFAULT '',
  `status` char(1) NOT NULL DEFAULT 'A',
  PRIMARY KEY (`store_location_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_stored_sessions;
CREATE TABLE `cscart_stored_sessions` (
  `session_id` varchar(32) NOT NULL,
  `expiry` int(11) unsigned NOT NULL,
  `data` text NOT NULL,
  `area` char(1) NOT NULL DEFAULT 'C',
  PRIMARY KEY (`session_id`,`area`),
  KEY `expiry` (`expiry`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_subscribers;
CREATE TABLE `cscart_subscribers` (
  `subscriber_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(128) NOT NULL DEFAULT '',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscriber_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_tag_links;
CREATE TABLE `cscart_tag_links` (
  `tag_id` mediumint(8) unsigned NOT NULL,
  `object_type` char(1) NOT NULL DEFAULT 'P',
  `object_id` mediumint(8) unsigned NOT NULL,
  `user_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`object_type`,`object_id`,`user_id`,`tag_id`),
  KEY `tag_id` (`tag_id`),
  KEY `user_id` (`user_id`),
  KEY `ids` (`tag_id`,`user_id`,`object_type`,`object_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_tags;
CREATE TABLE `cscart_tags` (
  `tag_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tag` varchar(255) NOT NULL DEFAULT '',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `status` char(1) NOT NULL DEFAULT 'P',
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tag` (`tag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_tax_descriptions;
CREATE TABLE `cscart_tax_descriptions` (
  `tax_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `tax` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_tax_rates;
CREATE TABLE `cscart_tax_rates` (
  `rate_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `tax_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `destination_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `apply_to` varchar(64) NOT NULL DEFAULT '',
  `rate_value` decimal(13,3) NOT NULL DEFAULT '0.000',
  `rate_type` char(1) NOT NULL DEFAULT '',
  `owner_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`rate_id`),
  UNIQUE KEY `tax_rate` (`tax_id`,`destination_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_taxes;
CREATE TABLE `cscart_taxes` (
  `tax_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `address_type` char(1) NOT NULL DEFAULT 'S',
  `status` char(1) NOT NULL DEFAULT 'D',
  `price_includes_tax` char(1) NOT NULL DEFAULT 'N',
  `display_including_tax` char(1) NOT NULL DEFAULT 'N',
  `display_info` char(1) NOT NULL DEFAULT '',
  `regnumber` varchar(255) NOT NULL DEFAULT '',
  `priority` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_id`),
  KEY `c_status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_user_data;
CREATE TABLE `cscart_user_data` (
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  PRIMARY KEY (`user_id`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_user_mailing_lists;
CREATE TABLE `cscart_user_mailing_lists` (
  `subscriber_id` mediumint(8) unsigned NOT NULL,
  `list_id` mediumint(8) unsigned NOT NULL,
  `activation_key` varchar(32) NOT NULL DEFAULT '',
  `unsubscribe_key` varchar(32) NOT NULL DEFAULT '',
  `confirmed` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `format` tinyint(3) unsigned NOT NULL DEFAULT '0',
  UNIQUE KEY `subscriber_list` (`list_id`,`subscriber_id`),
  KEY `subscriber_id` (`subscriber_id`),
  KEY `list_id` (`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_user_profiles;
CREATE TABLE `cscart_user_profiles` (
  `profile_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `profile_type` char(1) NOT NULL DEFAULT 'P',
  `b_title` varchar(32) NOT NULL DEFAULT '',
  `b_firstname` varchar(128) NOT NULL DEFAULT '',
  `b_lastname` varchar(128) NOT NULL DEFAULT '',
  `b_address` varchar(255) NOT NULL DEFAULT '',
  `b_address_2` varchar(255) NOT NULL DEFAULT '',
  `b_city` varchar(64) NOT NULL DEFAULT '',
  `b_county` varchar(32) NOT NULL DEFAULT '',
  `b_state` varchar(32) NOT NULL DEFAULT '',
  `b_country` char(2) NOT NULL DEFAULT '',
  `b_zipcode` varchar(16) NOT NULL DEFAULT '',
  `b_phone` varchar(32) NOT NULL DEFAULT '',
  `s_title` varchar(32) NOT NULL DEFAULT '',
  `s_firstname` varchar(128) NOT NULL DEFAULT '',
  `s_lastname` varchar(128) NOT NULL DEFAULT '',
  `s_address` varchar(255) NOT NULL DEFAULT '',
  `s_address_2` varchar(255) NOT NULL DEFAULT '',
  `s_city` varchar(255) NOT NULL DEFAULT '',
  `s_county` varchar(32) NOT NULL DEFAULT '',
  `s_state` varchar(32) NOT NULL DEFAULT '',
  `s_country` char(2) NOT NULL DEFAULT '',
  `s_zipcode` varchar(16) NOT NULL DEFAULT '',
  `s_phone` varchar(32) NOT NULL DEFAULT '',
  `s_address_type` varchar(255) NOT NULL DEFAULT '',
  `profile_name` varchar(32) NOT NULL DEFAULT '',
  `credit_cards` text NOT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `uid_p` (`user_id`,`profile_type`),
  KEY `profile_type` (`profile_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_user_session_products;
CREATE TABLE `cscart_user_session_products` (
  `user_id` int(11) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `type` char(1) NOT NULL DEFAULT 'C',
  `user_type` char(1) NOT NULL DEFAULT 'R',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `item_type` char(1) NOT NULL DEFAULT 'P',
  `product_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `amount` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `extra` text NOT NULL,
  `session_id` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`,`type`,`item_id`,`user_type`),
  KEY `timestamp` (`timestamp`,`user_type`),
  KEY `session_id` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_usergroup_descriptions;
CREATE TABLE `cscart_usergroup_descriptions` (
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `usergroup` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`usergroup_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_usergroup_links;
CREATE TABLE `cscart_usergroup_links` (
  `link_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `usergroup_id` mediumint(8) unsigned NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'D',
  PRIMARY KEY (`link_id`),
  UNIQUE KEY `user_id` (`user_id`,`usergroup_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_usergroup_privileges;
CREATE TABLE `cscart_usergroup_privileges` (
  `usergroup_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `privilege` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`usergroup_id`,`privilege`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_usergroups;
CREATE TABLE `cscart_usergroups` (
  `usergroup_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT '',
  `type` char(1) NOT NULL DEFAULT 'C',
  `recurring_plans_ids` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`usergroup_id`),
  KEY `c_status` (`usergroup_id`,`status`),
  KEY `status` (`status`,`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_users;
CREATE TABLE `cscart_users` (
  `user_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `status` char(1) NOT NULL DEFAULT 'A',
  `user_type` char(1) NOT NULL DEFAULT 'C',
  `user_login` varchar(255) NOT NULL DEFAULT '',
  `referer` varchar(255) NOT NULL DEFAULT '',
  `is_root` char(1) NOT NULL DEFAULT 'N',
  `company_id` int(11) unsigned NOT NULL DEFAULT '0',
  `last_login` int(11) unsigned NOT NULL DEFAULT '0',
  `timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  `password` varchar(32) NOT NULL DEFAULT '',
  `card_name` varchar(255) NOT NULL DEFAULT '',
  `card_type` varchar(16) NOT NULL DEFAULT '',
  `card_number` varchar(42) NOT NULL DEFAULT '',
  `card_expire` varchar(4) NOT NULL DEFAULT '',
  `card_cvv2` char(3) NOT NULL DEFAULT '',
  `title` varchar(24) NOT NULL DEFAULT '',
  `firstname` varchar(128) NOT NULL DEFAULT '',
  `lastname` varchar(128) NOT NULL DEFAULT '',
  `company` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `phone` varchar(32) NOT NULL DEFAULT '',
  `fax` varchar(32) NOT NULL DEFAULT '',
  `url` varchar(128) NOT NULL DEFAULT '',
  `tax_exempt` char(1) NOT NULL DEFAULT 'N',
  `lang_code` char(2) NOT NULL DEFAULT 'EN',
  `birthday` int(11) NOT NULL,
  `purchase_timestamp_from` int(11) NOT NULL DEFAULT '0',
  `purchase_timestamp_to` int(11) NOT NULL DEFAULT '0',
  `credit_value` decimal(12,2) NOT NULL DEFAULT '0.00',
  `responsible_email` varchar(80) NOT NULL DEFAULT '',
  `credit_used` decimal(12,2) NOT NULL DEFAULT '0.00',
  `last_passwords` varchar(255) NOT NULL DEFAULT '',
  `password_change_timestamp` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  KEY `user_login` (`user_login`),
  KEY `uname` (`title`,`firstname`,`lastname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS cscart_views;
CREATE TABLE `cscart_views` (
  `view_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `object` varchar(24) NOT NULL DEFAULT '',
  `name` varchar(32) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `view_results` text NOT NULL,
  `user_id` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `active` char(1) NOT NULL DEFAULT 'N',
  PRIMARY KEY (`view_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

